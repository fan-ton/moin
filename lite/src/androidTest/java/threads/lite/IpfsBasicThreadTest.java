package threads.lite;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.Cid;
import threads.lite.core.Session;

@RunWith(AndroidJUnit4.class)
public class IpfsBasicThreadTest {

    private static Context context;

    @BeforeClass
    public static void setup() {
        context = ApplicationProvider.getApplicationContext();
    }


    @Test
    public void test_sessionClose() throws Exception {

        IPFS ipfs = TestEnv.getTestInstance(context);

        Session session = ipfs.createSession();


        ExecutorService service = Executors.newSingleThreadExecutor();

        AtomicBoolean success = new AtomicBoolean(false);
        service.execute(() -> {
            try {
                Cid cid = ipfs.decodeCid("QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nt");
                ipfs.getData(session, cid, () -> false);
                fail(); // should not arrive herr
            } catch (Throwable throwable) {
                success.set(true);
            }
        });
        service.shutdown();
        Thread.sleep(10000);
        session.close(); // now close the session, this will close the service
        assertTrue(session.isClosed()); // session is closed check

        // we wait max 1 sec for the service to return
        boolean terminate = service.awaitTermination(1, TimeUnit.SECONDS);
        assertTrue(terminate);

        assertTrue(success.get());

    }
}
