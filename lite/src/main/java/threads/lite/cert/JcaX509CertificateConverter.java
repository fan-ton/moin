package threads.lite.cert;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;

/**
 * Converter for producing X509Certificate objects tied to a specific provider from X509CertificateHolder objects.
 */
public class JcaX509CertificateConverter {
    private final CertHelper helper;

    /**
     * Base constructor, configure with the default provider.
     */
    public JcaX509CertificateConverter() {
        this.helper = new DefaultCertHelper();
    }

    /**
     * Use the configured converter to produce a X509Certificate object from a X509CertificateHolder object.
     *
     * @param certHolder the holder to be converted
     * @return a X509Certificate object
     * @throws CertificateException if the conversion is unable to be made.
     */
    public X509Certificate getCertificate(X509CertificateHolder certHolder)
            throws CertificateException {
        try {
            CertificateFactory cFact = helper.getCertificateFactory("X.509");

            return (X509Certificate) cFact.generateCertificate(new ByteArrayInputStream(certHolder.getEncoded()));
        } catch (IOException e) {
            throw new ExCertificateParsingException("exception parsing certificate: " + e.getMessage(), e);
        } catch (NoSuchProviderException e) {
            throw new ExCertificateException("cannot find required provider:" + e.getMessage(), e);
        }
    }

    private static class ExCertificateParsingException
            extends CertificateParsingException {
        private final Throwable cause;

        public ExCertificateParsingException(String msg, Throwable cause) {
            super(msg);

            this.cause = cause;
        }

        public Throwable getCause() {
            return cause;
        }
    }

    private static class ExCertificateException
            extends CertificateException {
        private final Throwable cause;

        public ExCertificateException(String msg, Throwable cause) {
            super(msg);

            this.cause = cause;
        }

        public Throwable getCause() {
            return cause;
        }
    }
}