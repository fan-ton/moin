package threads.lite.cert;

import java.security.GeneralSecurityException;
import java.security.Signature;

import threads.lite.asn1.x509.AlgorithmIdentifier;

class OperatorHelper {

    private static String getSignatureName(AlgorithmIdentifier sigAlgId) {
        DefaultSignatureNameFinder sigFinder = new DefaultSignatureNameFinder();
        return sigFinder.getAlgorithmName(sigAlgId);
    }

    Signature createSignature(AlgorithmIdentifier sigAlgId) throws GeneralSecurityException {
        String sigName = getSignatureName(sigAlgId);
        return Signature.getInstance(sigName);
    }


}
