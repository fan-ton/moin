package threads.lite.cert;

import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;

import threads.lite.asn1.x509.AlgorithmIdentifier;

public class JcaContentSignerBuilder {

    private final String signatureAlgorithm;
    private final OperatorHelper helper = new OperatorHelper();

    public JcaContentSignerBuilder(String signatureAlgorithm) {
        this.signatureAlgorithm = signatureAlgorithm;
    }

    public ContentSigner build(PrivateKey privateKey) throws OperatorCreationException {

        try {
            AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder()
                    .find(signatureAlgorithm);


            final AlgorithmIdentifier signatureAlgId = sigAlgId;
            final Signature sig = helper.createSignature(sigAlgId);
            sig.initSign(privateKey);

            return new ContentSigner() {
                private final OutputStream stream = OutputStreamFactory.createStream(sig);

                public AlgorithmIdentifier getAlgorithmIdentifier() {
                    return signatureAlgId;
                }

                public OutputStream getOutputStream() {
                    return stream;
                }

                public byte[] getSignature() {
                    try {
                        return sig.sign();
                    } catch (SignatureException e) {
                        throw new RuntimeOperatorException("exception obtaining signature: " + e.getMessage(), e);
                    }
                }
            };
        } catch (GeneralSecurityException e) {
            throw new OperatorCreationException("cannot create signer: " + e.getMessage(), e);
        }
    }

}
