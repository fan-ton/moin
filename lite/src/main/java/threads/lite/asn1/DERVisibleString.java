package threads.lite.asn1;

/**
 * DER VisibleString object encoding ISO 646 (ASCII) character code points 32 to 126.
 * <p>
 * Explicit character set escape sequences are not allowed.
 * </p>
 */
public class DERVisibleString extends ASN1VisibleString {
    DERVisibleString(byte[] contents) {
        super(contents);
    }
}
