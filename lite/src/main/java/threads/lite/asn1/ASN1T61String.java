package threads.lite.asn1;

import androidx.annotation.NonNull;

import java.io.IOException;

import threads.lite.asn1.util.Arrays;
import threads.lite.asn1.util.Strings;

/**
 * ASN.1 T61String (also the teletex string), try not to use this if you don't need to. The standard support the encoding for
 * this has been withdrawn.
 */
public abstract class ASN1T61String extends ASN1Primitive implements ASN1String {
    final byte[] contents;

    ASN1T61String(byte[] contents) {
        this.contents = contents;
    }

    static ASN1T61String createPrimitive(byte[] contents) {
        return new DERT61String(contents);
    }

    /**
     * Decode the encoded string and return it, 8 bit encoding assumed.
     *
     * @return the decoded String
     */
    public final String getString() {
        return Strings.fromByteArray(contents);
    }

    @NonNull
    public String toString() {
        return getString();
    }

    final boolean encodeConstructed() {
        return false;
    }

    final int encodedLength(boolean withTag) {
        return ASN1OutputStream.getLengthOfEncodingDL(withTag, contents.length);
    }

    final void encode(ASN1OutputStream out, boolean withTag) throws IOException {
        out.writeEncodingDL(withTag, BERTags.T61_STRING, contents);
    }

    final boolean asn1Equals(ASN1Primitive other) {
        if (!(other instanceof ASN1T61String)) {
            return false;
        }

        ASN1T61String that = (ASN1T61String) other;

        return Arrays.areEqual(this.contents, that.contents);
    }

    public final int hashCode() {
        return Arrays.hashCode(contents);
    }
}
