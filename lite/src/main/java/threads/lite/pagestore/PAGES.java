package threads.lite.pagestore;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.Room;

import java.util.Date;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.IpnsEntity;
import threads.lite.core.PageStore;


public class PAGES implements PageStore {

    private static volatile PAGES INSTANCE = null;

    private final PageDatabase pageDatabase;

    private PAGES(PageDatabase pageDatabase) {
        this.pageDatabase = pageDatabase;
    }

    public static PAGES getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (PAGES.class) {
                if (INSTANCE == null) {
                    PageDatabase pageDatabase = Room.databaseBuilder(context,
                                    PageDatabase.class,
                                    PageDatabase.class.getSimpleName()).
                            fallbackToDestructiveMigration().build();

                    INSTANCE = new PAGES(pageDatabase);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void storePage(@NonNull IpnsEntity page) {
        pageDatabase.pageDao().insertPage(page);
    }

    @Override
    @Nullable
    public IpnsEntity getPage(@NonNull PeerId peerId) {
        return pageDatabase.pageDao().getPage(peerId);
    }

    @NonNull
    public PageDatabase getPageDatabase() {
        return pageDatabase;
    }


    @Override
    public void updatePageContent(@NonNull PeerId peerId, @NonNull Cid cid, @NonNull Date eol) {
        pageDatabase.pageDao().update(peerId, IpnsEntity.encodeIpnsData(cid), eol.getTime());
    }

    @Override
    public void clear() {
        getPageDatabase().clearAllTables();
    }

    @Override
    @Nullable
    public Cid getPageContent(@NonNull PeerId peerId) {
        try {
            byte[] data = getPageDatabase().pageDao().getValue(peerId);
            if (data != null) {
                return IpnsEntity.decodeIpnsData(data);
            }
        } catch (Throwable throwable) {
            throw new IllegalStateException(throwable);
        }
        return null;
    }

}
