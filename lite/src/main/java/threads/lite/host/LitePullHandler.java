package threads.lite.host;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;

import dht.pb.Dht;
import threads.lite.IPFS;
import threads.lite.core.IpnsRecord;
import threads.lite.core.ProtocolHandler;
import threads.lite.core.Stream;
import threads.lite.utils.DataHandler;

public class LitePullHandler implements ProtocolHandler {

    private final LiteHost host;

    public LitePullHandler(@NonNull LiteHost host) {
        this.host = host;
    }

    @Override
    public String getProtocol() {
        return IPFS.LITE_PULL_PROTOCOL;
    }

    @Override
    public void protocol(Stream stream) throws Exception {
        stream.writeOutput(DataHandler.encodeProtocols(IPFS.LITE_PULL_PROTOCOL));
        IpnsRecord ipnsRecord = host.getIpnsRecord();

        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat(
                IPFS.TIME_FORMAT_IPFS).format(new Date());

        Dht.Message.Record response = Dht.Message.Record.newBuilder()
                .setKey(ByteString.copyFrom(ipnsRecord.getIpnsKey()))
                .setValue(ByteString.copyFrom(ipnsRecord.getSealedRecord()))
                .setTimeReceived(format).build();

        stream.writeOutput(DataHandler.encode(response))
                .thenApply(Stream::closeOutput);
    }

    @Override
    public void data(Stream stream, ByteBuffer data) throws Exception {
        throw new Exception("should not be invoked");
    }
}
