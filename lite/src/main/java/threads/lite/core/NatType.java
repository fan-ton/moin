package threads.lite.core;

// FULL_CONE, RESTRICTED_CONE, PORT_RESTRICTED_CONE is RESTRICTED_CONE
// do not know how to distinguish them
public enum NatType {
    SYMMETRIC, RESTRICTED_CONE, UNKNOWN
}
