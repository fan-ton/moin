package threads.lite.utils;

import androidx.annotation.NonNull;

import com.google.protobuf.ByteString;

import java.util.Objects;

import merkledag.pb.Merkledag;
import threads.lite.cid.Cid;
import threads.lite.core.Cancellable;
import threads.lite.core.Session;
import threads.lite.dag.DagReader;
import threads.lite.dag.DagService;

public class Reader {

    private final DagReader dagReader;
    private final Cancellable cancellable;


    private Reader(@NonNull Cancellable cancellable, @NonNull DagReader dagReader) {
        this.cancellable = cancellable;
        this.dagReader = dagReader;
    }

    public static Reader getReader(@NonNull Cancellable cancellable,
                                   @NonNull Session session, @NonNull Cid cid)
            throws Exception {
        DagService dags = DagService.createDagService(session);
        Merkledag.PBNode top = Resolver.resolveNode(cancellable, dags, cid);
        Objects.requireNonNull(top);
        DagReader dagReader = DagReader.create(top, dags);

        return new Reader(cancellable, dagReader);
    }

    @NonNull
    public ByteString loadNextData() throws Exception {
        return dagReader.loadNextData(cancellable);
    }

    public void seek(long position) throws Exception {
        dagReader.seek(cancellable, position);
    }

    public long getSize() {
        return this.dagReader.getSize();
    }
}
