package threads.lite.quic;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import net.luminis.quic.QuicClientConnection;
import net.luminis.quic.QuicClientConnectionImpl;
import net.luminis.quic.QuicStream;
import net.luminis.quic.RawStreamData;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.Version;
import net.luminis.tls.CipherSuite;

import java.net.ConnectException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.net.ssl.X509TrustManager;

import threads.lite.IPFS;
import threads.lite.LogUtils;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.Parameters;
import threads.lite.core.StreamHandler;
import threads.lite.host.LiteCertificate;
import threads.lite.host.LiteConnection;
import threads.lite.host.LiteTrust;
import threads.lite.utils.PackageReader;

public class ConnectionBuilder {

    private static final String TAG = ConnectionBuilder.class.getSimpleName();
    @NonNull
    private static final AtomicInteger failure = new AtomicInteger(0);
    @NonNull
    private static final AtomicInteger success = new AtomicInteger(0);

    private InetSocketAddress remoteAddress;
    private Version quicVersion;
    private Integer initialRtt;
    private X509TrustManager trustManager;
    private X509Certificate clientCertificate;
    private PrivateKey clientCertificateKey;
    private TransportParameters transportParams;
    private String alpn;

    public static ConnectionBuilder newBuilder() {
        return new ConnectionBuilder();
    }

    @NonNull
    public static Connection connect(@NonNull StreamHandler streamHandler,
                                     @NonNull Multiaddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull LiteCertificate liteCertificate)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;
        PeerId peerId = address.getPeerId();
        QuicClientConnection connection = ConnectionBuilder.getConnection(streamHandler, address,
                parameters, liteCertificate, null);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            if (parameters.isKeepAlive()) {
                connection.keepAlive(IPFS.GRACE_PERIOD);
            }

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.warning(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    public static Connection connect(@NonNull StreamHandler streamHandler,
                                     @NonNull Multiaddr address,
                                     @NonNull Parameters parameters,
                                     @NonNull LiteCertificate liteCertificate,
                                     @NonNull DatagramSocket datagramSocket)
            throws ConnectException, InterruptedException, TimeoutException {

        long start = System.currentTimeMillis();
        boolean run = false;
        PeerId peerId = address.getPeerId();
        QuicClientConnection connection = ConnectionBuilder.getConnection(streamHandler, address,
                parameters, liteCertificate, datagramSocket);

        try {
            // now the remote peerId is available in Connection (LiteConnection)
            connection.setAttribute(Connection.REMOTE_PEER, peerId);
            connection.connect(IPFS.CONNECT_TIMEOUT);

            run = true;
            return new LiteConnection(connection);
        } finally {
            if (LogUtils.isError()) {
                if (run) {
                    success.incrementAndGet();
                } else {
                    failure.incrementAndGet();
                }
                LogUtils.warning(TAG, " Success " + run +
                        " (" + success.get() +
                        "," + failure.get() +
                        ") " + " Address " + address +
                        " Time " + (System.currentTimeMillis() - start));
            }
        }
    }

    @NonNull
    public static QuicClientConnection getConnection(@NonNull StreamHandler streamHandler,
                                                     @NonNull Multiaddr address,
                                                     @NonNull Parameters parameters,
                                                     @NonNull LiteCertificate liteCertificate,
                                                     @Nullable DatagramSocket datagramSocket)
            throws ConnectException {

        ConnectionBuilder builder = ConnectionBuilder.newBuilder()
                .clientCertificate(liteCertificate.x509Certificate())
                .clientCertificateKey(liteCertificate.getKey())
                .remoteAddress(address.getInetSocketAddress())
                .alpn(IPFS.ALPN)
                .transportParams(parameters);

        builder = builder.trustManager(new LiteTrust(address.getPeerId()));
        builder = builder.version(Version.QUIC_version_1);
        if (address.isAnyLocalAddress()) {
            builder = builder.initialRtt(100);
        }

        return builder.build(datagramSocket, quicStream -> new PackageReader(streamHandler));
    }

    public QuicClientConnectionImpl build(DatagramSocket datagramSocket,
                                          Function<QuicStream, Consumer<RawStreamData>> streamDataConsumer) throws ConnectException {

        if (remoteAddress == null) {
            throw new IllegalStateException("Cannot create connection when address is not set");
        }
        if (initialRtt != null && initialRtt < 1) {
            throw new IllegalArgumentException("Initial RTT must be larger than 0.");
        }
        if (transportParams == null) {
            throw new IllegalStateException("Connection requires transport parameters");
        }
        if (alpn == null) {
            throw new IllegalStateException("Connection requires application protocol");
        }
        if (quicVersion == null) {
            throw new IllegalStateException("Connection requires quic version");
        }
        try {
            QuicClientConnectionImpl quicConnection =
                    new QuicClientConnectionImpl(alpn, remoteAddress, quicVersion,
                            initialRtt, List.of(CipherSuite.TLS_AES_128_GCM_SHA256),
                            clientCertificate, clientCertificateKey, transportParams,
                            datagramSocket, streamDataConsumer);

            if (trustManager != null) {
                quicConnection.trustManager(trustManager);
            }

            return quicConnection;

        } catch (Throwable throwable) {
            throw new ConnectException(throwable.getMessage());
        }

    }

    public ConnectionBuilder alpn(String alpn) {
        this.alpn = alpn;
        return this;
    }

    public ConnectionBuilder transportParams(TransportParameters transportParams) {
        this.transportParams = transportParams;
        return this;
    }

    public ConnectionBuilder version(Version version) {
        quicVersion = version;
        return this;
    }

    public ConnectionBuilder remoteAddress(InetSocketAddress remoteAddress) {
        this.remoteAddress = remoteAddress;
        return this;
    }

    public ConnectionBuilder initialRtt(int initialRtt) {
        this.initialRtt = initialRtt;
        return this;
    }

    public ConnectionBuilder trustManager(X509TrustManager trustManager) {
        this.trustManager = trustManager;
        return this;
    }

    public ConnectionBuilder clientCertificate(X509Certificate certificate) {
        this.clientCertificate = certificate;
        return this;
    }

    public ConnectionBuilder clientCertificateKey(PrivateKey privateKey) {
        this.clientCertificateKey = privateKey;
        return this;
    }

}
