/*
 * Copyright © 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic;

import net.luminis.LogUtils;
import net.luminis.quic.packet.QuicPacket;

import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntSupplier;

public class IdleTimer {
    private static final String TAG = IdleTimer.class.getSimpleName();
    private final Timer timer;
    private final int timerResolution;
    private final QuicConnectionImpl connection;
    private final AtomicLong timeout = new AtomicLong();
    private final AtomicReference<IntSupplier> ptoSupplier = new AtomicReference<>(() -> 0);
    private final AtomicReference<Instant> lastAction = new AtomicReference<>(Instant.now());
    private final AtomicBoolean enabled = new AtomicBoolean(false);


    public IdleTimer(QuicConnectionImpl connection) {
        this(connection, 1000);
    }

    public IdleTimer(QuicConnectionImpl connection, int timerResolution) {
        this.connection = connection;
        this.timerResolution = timerResolution;
        this.timer = new Timer(true);
    }

    void setIdleTimeout(long idleTimeoutInMillis) {
        if (!enabled.getAndSet(true)) {
            timeout.set(idleTimeoutInMillis);
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    checkIdle();
                }
            }, timerResolution, timerResolution);
        } else {
            LogUtils.error(TAG, "idle timeout was set already; can't be set twice on same connection");
        }
    }

    public void setPtoSupplier(IntSupplier ptoSupplier) {
        this.ptoSupplier.set(ptoSupplier);
    }

    private void checkIdle() {
        if (enabled.get()) {
            Instant now = Instant.now();
            if (lastAction.get().plusMillis(timeout.get()).isBefore(now)) {
                int currentPto = ptoSupplier.get().getAsInt();
                // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
                // To avoid excessively small idle timeout periods, endpoints MUST increase the idle timeout period
                // to be at least three times the current Probe Timeout (PTO)
                if (lastAction.get().plusMillis(3L * currentPto).isBefore(now)) {
                    timer.cancel();
                    connection.silentlyCloseConnection(timeout.get() + currentPto);
                }
            }
        }
    }

    public void packetProcessed() {
        if (enabled.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint restarts its idle timer when a packet from its peer is received and processed successfully."
            lastAction.set(Instant.now());
        }
    }

    public void packetSent(QuicPacket packet, Instant sendTime) {
        if (enabled.get()) {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-31#section-10.1
            // "An endpoint also restarts its idle timer when sending an ack-eliciting packet if no other ack-eliciting
            //  packets have been sent since last receiving and processing a packet. "
            if (packet.isAckEliciting()) {
                lastAction.set(sendTime);
            }
        }
    }

    public void shutdown() {
        if (enabled.get()) {
            timer.cancel();
        }
    }
}

