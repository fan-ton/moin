/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Kwik, an implementation of the QUIC protocol in Java.
 *
 * Kwik is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Kwik is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package net.luminis.quic.stream;

import static net.luminis.quic.QuicConstants.TransportErrorCode.STREAM_STATE_ERROR;

import net.luminis.LogUtils;
import net.luminis.quic.ImplementationError;
import net.luminis.quic.QuicStream;
import net.luminis.quic.Role;
import net.luminis.quic.TransportError;
import net.luminis.quic.TransportParameters;
import net.luminis.quic.frame.MaxDataFrame;
import net.luminis.quic.frame.MaxStreamDataFrame;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Keeps track of connection and stream flow control limits imposed by the peer.
 */
public class FlowControl {
    private static final String TAG = FlowControl.class.getSimpleName();
    private final Role role;

    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-18.2
    // "The initial maximum data parameter is an integer value that contains the initial value for the maximum
    //  amount of data that can be sent on the connection."
    private final AtomicLong initialMaxData = new AtomicLong();
    // "initial_max_stream_data_bidi_local (0x0005):  This parameter is an integer value specifying the initial flow control limit for
    //  locally-initiated bidirectional streams.
    private final AtomicLong initialMaxStreamDataBidiLocal = new AtomicLong();
    // "initial_max_stream_data_bidi_remote (0x0006):  This parameter is an integer value specifying the initial flow control limit for peer-
    //  initiated bidirectional streams. "
    private final AtomicLong initialMaxStreamDataBidiRemote = new AtomicLong();
    // "initial_max_stream_data_uni (0x0007):  This parameter is an integer value specifying the initial flow control limit for unidirectional
    //  streams."
    private final AtomicLong initialMaxStreamDataUni = new AtomicLong();

    private final Map<Integer, FlowControlUpdateListener> streamListeners = new ConcurrentHashMap<>();
    // The maximum amount of data that a stream would be allowed to send (to the peer), ignoring possible connection limit
    private final Map<Integer, Long> maxStreamDataAllowed = new ConcurrentHashMap<>();
    // The maximum amount of data that is already assigned to a stream (i.e. already sent, or upon being sent)
    private final Map<Integer, Long> maxStreamDataAssigned = new ConcurrentHashMap<>();
    private final AtomicInteger maxOpenedStreamId = new AtomicInteger(0);
    // The maximum amount of data that can be sent (to the peer) on the connection as a whole
    private final AtomicLong maxDataAllowed = new AtomicLong();
    // The maximum amount of data that can be sent on the connection, that is already assigned to a particular stream
    private final AtomicLong maxDataAssigned = new AtomicLong(0);
    private final AtomicBoolean init = new AtomicBoolean(false);

    public FlowControl(Role role) {
        this.role = role;
    }

    public boolean isInit() {
        return init.get();
    }

    public void init(long initialMaxData, long initialMaxStreamDataBidiLocal,
                     long initialMaxStreamDataBidiRemote, long initialMaxStreamDataUni) {
        this.initialMaxData.set(initialMaxData);
        this.initialMaxStreamDataBidiLocal.set(initialMaxStreamDataBidiLocal);
        this.initialMaxStreamDataBidiRemote.set(initialMaxStreamDataBidiRemote);
        this.initialMaxStreamDataUni.set(initialMaxStreamDataUni);
        this.init.set(true);
        this.maxDataAllowed.set(initialMaxData);
    }

    // todo maybe (have removed a function in QuicConnection)
    public long getInitialMaxData() {
        return initialMaxData.get();
    }

    /**
     * Request to increase the flow control limit for the indicated stream to the indicated value. Whether this is
     * possible depends on whether the stream flow control limit allows this and whether the connection flow control
     * limit has enough "unused" credits.
     *
     * @return the new flow control limit for the stream: the offset of the last byte sent on the stream may not past this limit.
     */
    public long increaseFlowControlLimit(int streamId, long requestedLimit) {

        Long currentMasStreamDataAssigned = maxStreamDataAssigned.get(streamId);
        Long currentMaxStreamDataAllowed = maxStreamDataAllowed.get(streamId);

        if (currentMasStreamDataAssigned != null && currentMaxStreamDataAllowed != null) {
            long possibleStreamIncrement = currentStreamCredits(
                    currentMasStreamDataAssigned, currentMaxStreamDataAllowed);
            long requestedIncrement = requestedLimit - currentMasStreamDataAssigned;
            long proposedStreamIncrement = Long.min(requestedIncrement, possibleStreamIncrement);

            if (requestedIncrement < 0) {
                throw new IllegalArgumentException();
            }

            maxDataAssigned.getAndAdd(proposedStreamIncrement);
            long newStreamLimit = currentMasStreamDataAssigned + proposedStreamIncrement;
            maxStreamDataAssigned.put(streamId, newStreamLimit);

            return newStreamLimit;
        }
        return -1;
    }

    /**
     * Returns the maximum flow control limit for the given stream, if it was requested now. Note that this limit
     * cannot be used to send data on the stream, as the flow control credits are not yet reserved.
     */
    public long getFlowControlLimit(int streamId) {
        Long currentMasStreamDataAssigned = maxStreamDataAssigned.get(streamId);
        Long currentMaxStreamDataAllowed = maxStreamDataAllowed.get(streamId);
        if (currentMasStreamDataAssigned != null && currentMaxStreamDataAllowed != null) {
            return currentMasStreamDataAssigned + currentStreamCredits(
                    currentMasStreamDataAssigned, currentMaxStreamDataAllowed);
        }
        return -1;
    }

    /**
     * Returns the reason why a given stream is blocked, which can be due that the stream flow control limit is reached
     * or the connection data limit is reached.
     */
    public BlockReason getFlowControlBlockReason(int streamId) {
        if (Objects.equals(maxStreamDataAssigned.get(streamId),
                maxStreamDataAllowed.get(streamId))) {
            return BlockReason.STREAM_DATA_BLOCKED;
        }
        if (maxDataAllowed.get() == maxDataAssigned.get()) {
            return BlockReason.DATA_BLOCKED;
        }

        return BlockReason.NOT_BLOCKED;
    }

    /**
     * Returns the current connection flow control limit.
     *
     * @return current connection flow control limit
     */
    public long getConnectionDataLimit() {
        return maxDataAllowed.get();
    }

    /**
     * Update initial values. This can happen in a client that has sent 0-RTT data, for which it has used remembered
     * values and that updates the values when the ServerHello message is received.
     * Hence: only called by a client.
     */
    public void updateInitialValues(TransportParameters peerTransportParameters) {

        if (peerTransportParameters.getInitialMaxData() > initialMaxData.get()) {
            LogUtils.info(TAG, "Increasing initial max data from " +
                    initialMaxData + " to " + peerTransportParameters.getInitialMaxData());
            if (peerTransportParameters.getInitialMaxData() > maxDataAllowed.get()) {
                maxDataAllowed.set(peerTransportParameters.getInitialMaxData());
            }
        } else if (peerTransportParameters.getInitialMaxData() < initialMaxData.get()) {
            LogUtils.error(TAG, "Ignoring attempt to reduce initial max data from " +
                    initialMaxData + " to " + peerTransportParameters.getInitialMaxData());
        }

        if (peerTransportParameters.getInitialMaxStreamDataBidiLocal() >
                initialMaxStreamDataBidiLocal.get()) {
            LogUtils.info(TAG, "Increasing initial max data from " +
                    initialMaxStreamDataBidiLocal + " to " +
                    peerTransportParameters.getInitialMaxStreamDataBidiLocal());

            maxStreamDataAllowed.entrySet().stream()
                    // Find all server initiated bidirectional streams
                    .filter(entry -> entry.getKey() % 4 == 1)
                    .forEach(entry -> {
                        if (peerTransportParameters.getInitialMaxStreamDataBidiLocal() > entry.getValue()) {
                            maxStreamDataAllowed.put(entry.getKey(), peerTransportParameters.getInitialMaxStreamDataBidiLocal());
                        }
                    });
        } else if (peerTransportParameters.getInitialMaxStreamDataBidiLocal() <
                initialMaxStreamDataBidiLocal.get()) {
            LogUtils.error(TAG, "Ignoring attempt to reduce max data from " +
                    initialMaxStreamDataBidiLocal + " to " + peerTransportParameters.getInitialMaxStreamDataBidiLocal());
        }

        if (peerTransportParameters.getInitialMaxStreamDataBidiRemote() >
                initialMaxStreamDataBidiRemote.get()) {
            LogUtils.info(TAG, "Increasing initial max data from " + initialMaxStreamDataBidiRemote +
                    " to " + peerTransportParameters.getInitialMaxStreamDataBidiRemote());
            maxStreamDataAllowed.entrySet().stream()
                    // Find all client initiated bidirectional streams
                    .filter(entry -> entry.getKey() % 4 == 0)
                    .forEach(entry -> {
                        if (peerTransportParameters.getInitialMaxStreamDataBidiRemote() > entry.getValue()) {
                            maxStreamDataAllowed.put(entry.getKey(),
                                    peerTransportParameters.getInitialMaxStreamDataBidiRemote());
                        }
                    });
        } else if (peerTransportParameters.getInitialMaxStreamDataBidiRemote() <
                initialMaxStreamDataBidiRemote.get()) {
            LogUtils.error(TAG, "Ignoring attempt to reduce max data from " +
                    initialMaxStreamDataBidiRemote + " to " +
                    peerTransportParameters.getInitialMaxStreamDataBidiRemote());
        }

        if (peerTransportParameters.getInitialMaxStreamDataUni() > initialMaxStreamDataUni.get()) {
            LogUtils.info(TAG, "Increasing initial max data from " +
                    initialMaxStreamDataUni + " to " + peerTransportParameters.getInitialMaxStreamDataUni());
            maxStreamDataAllowed.entrySet().stream()
                    // Find all client initiated unidirectional streams
                    .filter(entry -> entry.getKey() % 4 == 2)
                    .forEach(entry -> {
                        if (peerTransportParameters.getInitialMaxStreamDataUni() > entry.getValue()) {
                            maxStreamDataAllowed.put(entry.getKey(), peerTransportParameters.getInitialMaxStreamDataUni());
                        }
                    });
        } else if (peerTransportParameters.getInitialMaxStreamDataUni() < initialMaxStreamDataUni.get()) {
            LogUtils.error(TAG, "Ignoring attempt to reduce max data from " +
                    initialMaxStreamDataUni + " to " + peerTransportParameters.getInitialMaxStreamDataUni());
        }
    }

    public void register(int streamId, FlowControlUpdateListener listener) {
        streamListeners.put(streamId, listener);
    }

    public void unregister(int streamId) {
        streamListeners.remove(streamId);
    }


    public void streamOpened(QuicStream stream) {
        int streamId = stream.getStreamId();

        if (!maxStreamDataAllowed.containsKey(streamId)) {
            maxStreamDataAllowed.put(streamId, determineInitialMaxStreamData(stream));
            maxStreamDataAssigned.put(streamId, 0L);
        }
        maxOpenedStreamId.updateAndGet(i -> Math.max(streamId, i));

    }

    public void streamClosed(int streamId) {
        maxStreamDataAssigned.remove(streamId);
        maxStreamDataAllowed.remove(streamId);
    }

    private long determineInitialMaxStreamData(QuicStream stream) {
        if (stream.isUnidirectional()) {
            return initialMaxStreamDataUni.get();
        } else if (role == Role.Client && stream.isClientInitiatedBidirectional()
                || role == Role.Server && stream.isServerInitiatedBidirectional()) {
            // For the receiver (imposing the limit) the stream is peer-initiated (remote).
            // "This limit applies to newly created bidirectional streams opened by the endpoint that receives
            // the transport parameter."
            return initialMaxStreamDataBidiRemote.get();
        } else if (role == Role.Client && stream.isServerInitiatedBidirectional()
                || role == Role.Server && stream.isClientInitiatedBidirectional()) {
            // For the receiver (imposing the limit), the stream is locally-initiated
            // "This limit applies to newly created bidirectional streams opened by the endpoint that sends the
            // transport parameter."
            return initialMaxStreamDataBidiLocal.get();
        } else {
            throw new ImplementationError();
        }
    }

    /**
     * Returns the maximum possible flow control limit for the given stream, taking into account both stream and connection
     * flow control limits. Note that the returned limit is not yet reserved for use by this stream!
     */
    private long currentStreamCredits(long maxStreamDataAssigned, long maxStreamDataAllowed) {
        long maxStreamIncrement = maxStreamDataAllowed - maxStreamDataAssigned;
        long maxPossibleDataIncrement = maxDataAllowed.get() - maxDataAssigned.get();
        if (maxStreamIncrement > maxPossibleDataIncrement) {
            maxStreamIncrement = maxPossibleDataIncrement;
        }
        return maxStreamIncrement;
    }

    public void process(MaxDataFrame frame) {

        // If frames are received out of order, the new max can be smaller than the current value.
        long currentMaxDataAllowed = maxDataAllowed.get();
        if (frame.getMaxData() > currentMaxDataAllowed) {
            boolean maxDataWasReached = currentMaxDataAllowed == maxDataAssigned.get();
            maxDataAllowed.set(frame.getMaxData());
            if (maxDataWasReached) {
                streamListeners.forEach((streamId, listener) -> {
                    boolean streamWasBlockedByMaxDataOnly = !Objects.equals(
                            maxStreamDataAssigned.get(streamId), maxStreamDataAllowed.get(streamId));
                    if (streamWasBlockedByMaxDataOnly) {
                        listener.streamNotBlocked(streamId);
                    }
                });
            }
        }
    }

    public void process(MaxStreamDataFrame frame) throws TransportError {

        int streamId = frame.getStreamId();
        long maxStreamData = frame.getMaxData();
        Long currentMaxStreamDataAllowed = maxStreamDataAllowed.get(streamId);
        if (currentMaxStreamDataAllowed != null) {
            // If frames are received out of order, the new max can be smaller than the current value.
            if (maxStreamData > currentMaxStreamDataAllowed) {
                Long maxStreamDataAssignedCwnd = maxStreamDataAssigned.get(streamId);
                if (maxStreamDataAssignedCwnd != null) {
                    boolean streamWasBlocked = maxStreamDataAssignedCwnd.longValue()
                            == currentMaxStreamDataAllowed.longValue()
                            && maxDataAssigned.get() != maxDataAllowed.get();
                    maxStreamDataAllowed.put(streamId, maxStreamData);
                    if (streamWasBlocked) {
                        FlowControlUpdateListener streamListener = streamListeners.get(streamId);
                        if (streamListener != null) {
                            streamListener.streamNotBlocked(streamId);
                        }
                    }
                }
            }
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-33#section-19.10
            // "Receiving a MAX_STREAM_DATA frame for a locally-initiated stream that has not yet been created MUST
            //  be treated as a connection error of type STREAM_STATE_ERROR."
            if (locallyInitiated(streamId) && streamId > maxOpenedStreamId.get()) {
                throw new TransportError(STREAM_STATE_ERROR);
            }
        }

    }

    private boolean locallyInitiated(int streamId) {
        if (role == Role.Client) {
            return streamId % 2 == 0;
        } else {
            return streamId % 2 == 1;
        }
    }

    public long getInitialMaxStreamDataBidiRemote() {

        return initialMaxStreamDataBidiRemote.get();
    }

}
