# **moin**


## What is **moin** ?

**moin** is a decentralized peer to peer application with the focus of sharing files among users. 
The basic characteristics are decentralized, respect of personal data, open source, 
free of charge, free of advertising and legally impeccable.

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
     alt="Get it on Google Play"
     height="80">](https://play.google.com/store/apps/details?id=threads.app.basic)

## Table of Contents
- [System Requirements](#system-requirements)
- [How it works](#how-it-works)
- [Security](#security)
- [Legal Notices](#legal-notices)
- [Data Privacy](#data-privacy)
- [Privacy Policy](#privacy-policy)
    - [Data Protection](#data-protection)
    - [Android Permissions](#android-permissions)



## System Requirements
An Android runtime environment with SDK 26 is required.

Support OS :
- [x] Android OS SDK >= 29
- [x] Chrome OS devices with Android support

Supported devices :
- [x] Smartphone
- [x] Tablet 
- [x] Chrome OS devices with Android support

Android App-Stores :
- [x] Google Play Store
- [x] F-Droid
- [x] Huawei 
- ...

## How it works
This section describes briefly how the application works and the main features of the application.
The application itself based on **IPFS** technology (see https://ipfs.io/).


### Identification
Each user of the application gets an unique ID. In the **moin** application it is called 
**Identification** (in **IPFS** terminology this is called Peer ID).

The **Identification** is required, so that users can find each other.

In order to have a data exchange between users it is required that a user adds the 
**Identification** of the other users in advance in the contact view.

There are currently only one way supported how to share the **Identification**
- [x] Direct Identification Sharing via QR code
    
    In this scenario both users share the **Identification** via a QR code
    
    One user scans the QR code of the other user and vice versa


**Note** When there is no direct connection, the data exchange will be done over so called relays. 
Which results in slower data exchange and connection problems in general. 
See section **Port Forwarding** to improve your visibility when you are connected via WLAN.


### Profile
The user has the possibility to provide a name (alias). This name will be presented to the other
users you are communicating with. This name setting can be done in the main navigation bar 
of the application (called **Profile**).


### Server Mode
The application runs always in **Server Mode**. The reasons are :
- the application is run as a foreground service and therefore will not be closed by the Android system
- the user always provide his data to the others users who are defined in the contact view
- the user always can get notifications from the other who are defined in the contact view


## Security
The **moin** application based on the IPFS security technology. 

Means when two nodes (users) are changing data with each other, the application uses the
end-to-end encryption (TLS and Noise) of the underlying IPFS technology.

### Data Exchange
In addition to end-to-end encryption mechanism, data can only be shared **directly** to
those peers which are defined in your contact overview.

Moreover the data change algorithm of IPFS (**ipfs-bitswap**) has been adapted so that only
data change is possible between peers which known each other.
In the application the known peers are shown in the contact overview.

This leads to the situation that a connection to a regular **IPFS** peer is possible, but 
data can not be exchanged between them.


## Legal Notices
In general the idea is that the user is responsible for everything, because the provider of this
app does not run any server. It just provides a platform which can be used. Also no financial 
interests are involved. 

## Data Privacy
This section describes the data privacy from the users perspective.

__Who can see my personal information__
- User Name (Alias)

  Any participant of the application who is connected with the user directly.
- Content Data

  Your content data can only be accessed by the peers which are defined in your contact overview.

  The communication itself is protected by an end-to-end encryption which is provided by the
  underlying IPFS technology.

## Links

[Privacy Policy](https://gitlab.com/remmer.wilts/moin/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/remmer.wilts/moin/-/blob/master/LICENSE)
