<b>moin</b> is a decentralized peer-to-peer application with the focus of sharing files among users.
The basic characteristics are decentralized, respect of personal data, open source, free of charge,
free of advertising and legally impeccable.
The application based on IPFS technologies to fulfill the decentralized aspect of the application.