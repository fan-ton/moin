package threads.moin.mails;

import android.media.MediaPlayer;

import androidx.annotation.NonNull;

import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.PeerId;
import threads.moin.LogUtils;
import threads.moin.core.mails.Mail;

public interface MailAdapterListener {
    String TAG = MailAdapterListener.class.getSimpleName();
    @NonNull
    AtomicReference<AudioHolder> audioHolder = new AtomicReference<>(null);

    void invokeCopyTo(@NonNull Mail mail);

    void invokeDownloadAction(@NonNull Mail mail);

    void invokeAction(@NonNull Mail mail);

    void invokePauseAction(@NonNull Mail mail);

    void invokeUploadAction(@NonNull Mail mail);

    String getAlias();

    PeerId self();

    String selfName();

    MediaPlayer getMediaPlayer();

    default void stopAudioHolder() {
        try {
            AudioHolder audioHolder = this.audioHolder.get();
            if (audioHolder != null) {
                audioHolder.stop();
            }
            if (getMediaPlayer().isPlaying()) {
                getMediaPlayer().stop();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
