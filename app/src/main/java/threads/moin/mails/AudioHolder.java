package threads.moin.mails;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.slider.Slider;

import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.mails.Mail;

public class AudioHolder extends MailHolder {

    final TextView user;
    final ImageView general_action;
    final Slider slider;
    final TextView time;


    public AudioHolder(View v, Context context, MailAdapterListener listener) {
        super(v, context, listener);
        general_action = v.findViewById(R.id.general_action);
        user = v.findViewById(R.id.peer);
        slider = v.findViewById(R.id.seek_bar);
        time = v.findViewById(R.id.time);

    }

    @Override
    public void bind(@NonNull Mail mail) {
        if (Objects.equals(listener.self(), mail.getOwner())) {
            user.setText(listener.selfName());
            handleEnhancedDate(mail);
        } else {
            user.setText(listener.getAlias());
            handleDate(mail);
        }

        if (!Objects.equals(listener.self(), mail.getOwner())) {
            if (mail.isLeaching()) {
                general_action.setImageResource(R.drawable.pause);
                general_action.setVisibility(View.VISIBLE);
                general_action.setOnClickListener((v) ->
                        listener.invokePauseAction(mail)
                );
            } else if (mail.isSeeding()) {
                general_action.setVisibility(View.VISIBLE);
                handleAudio(mail);
            } else {

                general_action.setVisibility(View.VISIBLE);
                general_action.setImageResource(R.drawable.download);
                general_action.setOnClickListener((v) ->
                        listener.invokeDownloadAction(mail)
                );

            }
        } else {
            if (mail.isSeeding()) {
                general_action.setVisibility(View.VISIBLE);
                handleAudio(mail);
            }
        }
    }


    private void handleAudio(@NonNull Mail mail) {

        MediaPlayer mediaPlayer = listener.getMediaPlayer();
        general_action.setOnClickListener((v) -> {
            try {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    stop();
                } else {
                    listener.stopAudioHolder();
                    play();
                    listener.audioHolder.set(this);

                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.setDataSource(context, Uri.parse(mail.getUri()));
                    mediaPlayer.setOnCompletionListener(mp -> done());
                    mediaPlayer.prepare();
                    mediaPlayer.seekTo((int) slider.getValue());
                    mediaPlayer.start();


                    ExecutorService executor = Executors.newSingleThreadExecutor();
                    executor.submit(() -> {
                        try {
                            do {
                                if (mediaPlayer.isPlaying()) {
                                    int progress = mediaPlayer.getCurrentPosition();
                                    slider.setValue(progress);
                                }
                            } while (mediaPlayer.isPlaying());


                        } catch (Throwable throwable) {
                            LogUtils.error(TAG, throwable);
                        }
                    });
                }


                int duration = mediaPlayer.getDuration();
                slider.setValueTo(duration);
                time.setText(getTime(duration));
                slider.addOnChangeListener((slider, value, fromUser) -> {
                    if (fromUser) {
                        int progress = (int) value;
                        mediaPlayer.seekTo(progress);
                        LogUtils.info(TAG, "Progress : " + progress);

                    }
                });

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
    }

    private String getTime(int duration) {
        return DateUtils.formatElapsedTime(TimeUnit.MILLISECONDS.toSeconds(duration));
    }


    void stop() {
        general_action.setImageResource(R.drawable.play_circle);
    }

    void play() {
        general_action.setImageResource(R.drawable.pause_circle);
    }


    void done() {
        try {
            stop();
            slider.setValue(0);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
