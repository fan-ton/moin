package threads.moin.mails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import threads.moin.R;
import threads.moin.core.mails.Mail;

public class MailViewAdapter extends RecyclerView.Adapter<MailHolder> {
    @NonNull
    private final Context context;
    @NonNull
    private final MailAdapterListener listener;
    private final List<Mail> mails = new ArrayList<>();

    public MailViewAdapter(@NonNull Context context, @NonNull MailAdapterListener listener) {
        this.context = context;
        this.listener = listener;
    }


    @NonNull
    @Override
    public MailHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        if (viewType == R.layout.note_data_load) {
            return new DataLoadHolder(v, context, listener);
        } else if (viewType == R.layout.note_info) {
            return new InfoHolder(v, context, listener);
        } else if (viewType == R.layout.note_message_own) {
            return new OwnMessageHolder(v, context, listener);
        } else if (viewType == R.layout.note_audio_own) {
            return new AudioHolder(v, context, listener);
        } else if (viewType == R.layout.note_audio) {
            return new AudioHolder(v, context, listener);
        } else if (viewType == R.layout.note_info_own) {
            return new OwnInfoHolder(v, context, listener);
        } else if (viewType == R.layout.note_message) {
            return new MessageHolder(v, context, listener);
        } else if (viewType == R.layout.note_file) {
            return new FileHolder(v, context, listener);
        } else if (viewType == R.layout.note_file_own) {
            return new OwnFileHolder(v, context, listener);
        } else {
            throw new RuntimeException("View Type not supported !!!");
        }
    }


    public void updateData(@NonNull List<Mail> mails) {

        final MailDiffCallback diffCallback = new MailDiffCallback(this.mails, mails);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.mails.clear();
        this.mails.addAll(mails);
        diffResult.dispatchUpdatesTo(this);


    }

    @Override
    public void onBindViewHolder(@NonNull MailHolder holder, int position) {
        Mail item = getItem(position);
        Objects.requireNonNull(item);
        holder.bind(item);
    }

    private Mail getItem(int position) {
        return mails.get(position);
    }

    @Override
    public int getItemViewType(int position) {
        Mail mail = getItem(position);
        Objects.requireNonNull(mail);

        if (mail.getType() == Mail.Type.INFO) {
            if (Objects.equals(mail.getOwner(), listener.self())) {
                return R.layout.note_info_own;
            } else {
                return R.layout.note_info;
            }
        } else if (mail.getType() == Mail.Type.MESSAGE) {
            if (Objects.equals(mail.getOwner(), listener.self())) {
                return R.layout.note_message_own;
            } else {
                if (!mail.isSeeding()) {
                    return R.layout.note_data_load;
                } else {
                    return R.layout.note_message;
                }
            }
        } else if (mail.getType() == Mail.Type.AUDIO) {
            if (Objects.equals(mail.getOwner(), listener.self())) {
                return R.layout.note_audio_own;
            } else {
                if (!mail.isSeeding()) {
                    return R.layout.note_data_load;
                } else {
                    return R.layout.note_audio;
                }
            }
        } else if (mail.getType() == Mail.Type.FILE) {
            if (Objects.equals(mail.getOwner(), listener.self())) {
                return R.layout.note_file_own;
            } else {
                return R.layout.note_file;
            }
        } else {
            // all other stuff is info
            if (Objects.equals(mail.getOwner(), listener.self())) {
                return R.layout.note_info_own;
            } else {
                return R.layout.note_info;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mails.size();
    }
}
