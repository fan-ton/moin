package threads.moin.mails;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import threads.moin.R;
import threads.moin.core.mails.Mail;

public class MessageHolder extends MailHolder {

    final TextView user;
    final TextView message_body;


    public MessageHolder(View v, Context context, MailAdapterListener listener) {
        super(v, context, listener);


        message_body = v.findViewById(R.id.message_body);
        user = v.findViewById(R.id.peer);

    }

    @Override
    public void bind(@NonNull Mail mail) {

        user.setText(listener.getAlias());

        handleDate(mail);
        message_body.setText(mail.getText());
    }
}
