package threads.moin.mails;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Date;

import threads.moin.R;
import threads.moin.core.mails.Mail;

public abstract class MailHolder extends RecyclerView.ViewHolder {
    protected static final String TAG = MailHolder.class.getSimpleName();
    final TextView date;
    final View view;
    final Context context;
    final MailAdapterListener listener;

    MailHolder(View v, Context context, MailAdapterListener listener) {
        super(v);
        this.context = context;
        this.listener = listener;
        view = v;
        date = v.findViewById(R.id.date);

    }

    public abstract void bind(@NonNull Mail mail);


    String getFileSize(@NonNull Mail mail) {

        String fileSize;
        long size = mail.getSize();

        if (size < 1000) {
            fileSize = String.valueOf(size);
            return fileSize.concat(" B");
        } else if (size < 1000 * 1000) {
            fileSize = String.valueOf((double) (size / 1000));
            return fileSize.concat(" KB");
        } else {
            fileSize = String.valueOf((double) (size / (1000 * 1000)));
            return fileSize.concat(" MB");
        }
    }

    @NonNull
    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    void handleDate(@NonNull Mail mail) {
        date.setText(getDate(new Date(mail.getDate())));
    }

    void handleEnhancedDate(@NonNull Mail mail) {


        date.setText(getDate(new Date(mail.getDate())));

        int res = 0;

        if (mail.isSeeding()) {
            if (mail.isReceived()) {
                res = R.drawable.check_all;
            } else if (mail.isPublished()) {
                res = R.drawable.check;
            }
        }

        date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                0, 0, res, 0);

    }

}