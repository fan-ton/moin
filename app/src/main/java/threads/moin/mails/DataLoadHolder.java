package threads.moin.mails;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.progressindicator.LinearProgressIndicator;

import threads.moin.R;
import threads.moin.core.mails.Mail;

public class DataLoadHolder extends MailHolder {

    final TextView user;
    final TextView message_body;
    final ImageView general_action;
    final ImageView image_view;
    final TextView size;
    final LinearProgressIndicator progress_bar;

    public DataLoadHolder(View v, Context context, MailAdapterListener listener) {
        super(v, context, listener);


        general_action = v.findViewById(R.id.general_action);
        user = v.findViewById(R.id.peer);
        message_body = v.findViewById(R.id.message_body);
        image_view = v.findViewById(R.id.image_view);
        progress_bar = v.findViewById(R.id.progress_bar);
        size = v.findViewById(R.id.size);
    }

    @Override
    public void bind(@NonNull Mail mail) {

        user.setText(listener.getAlias());

        handleDate(mail);


        int res = R.drawable.forum_outline;

        if (mail.getType() == Mail.Type.AUDIO) {
            res = R.drawable.audio;
        } else if (mail.getType() == Mail.Type.MESSAGE) {
            res = R.drawable.email_outline;
        }
        image_view.setImageResource(res);


        String text = mail.getText();
        if (text.isEmpty()) {
            if (mail.getType() == Mail.Type.AUDIO) {
                text = context.getString(R.string.audio);
            } else if (mail.getType() == Mail.Type.MESSAGE) {
                text = context.getString(R.string.message);
            } else {
                text = "";
            }
        }

        message_body.setText(text);

        String fileSize = getFileSize(mail);
        size.setText(fileSize);


        if (mail.isLeaching()) {
            progress_bar.setVisibility(View.VISIBLE);
            general_action.setImageResource(R.drawable.pause);
            general_action.setVisibility(View.VISIBLE);
            general_action.setOnClickListener((v) ->
                    listener.invokePauseAction(mail)
            );
        } else {
            progress_bar.setVisibility(View.INVISIBLE);
            general_action.setVisibility(View.VISIBLE);
            general_action.setImageResource(R.drawable.download);
            general_action.setOnClickListener((v) ->
                    listener.invokeDownloadAction(mail)
            );

        }
    }
}
