package threads.moin.mails;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.moin.core.mails.Mail;

@SuppressWarnings("WeakerAccess")
public class MailDiffCallback extends DiffUtil.Callback {
    private final List<Mail> mOldList;
    private final List<Mail> mNewList;

    public MailDiffCallback(List<Mail> messages, List<Mail> messageBoxes) {
        this.mOldList = messages;
        this.mNewList = messageBoxes;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).sameContent(mNewList.get(newItemPosition));
    }


}
