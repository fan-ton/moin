package threads.moin.boxes;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.color.MaterialColors;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.MimeType;
import threads.moin.core.TextDrawable;
import threads.moin.core.boxes.Box;
import threads.moin.services.MimeTypeService;


public class BoxViewAdapter extends RecyclerView.Adapter<BoxViewAdapter.ViewHolder> implements BoxItemPosition {

    private static final String TAG = BoxViewAdapter.class.getSimpleName();
    private final Context mContext;
    private final ViewAdapterListener mListener;
    private final List<Box> boxes = new ArrayList<>();
    @Nullable
    private SelectionTracker<Long> mSelectionTracker;


    public BoxViewAdapter(@NonNull Context context,
                          @NonNull ViewAdapterListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.box;
    }

    @Override
    @NonNull
    public BoxViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                        int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new BoxViewHolder(this, v);
    }

    long getIdx(int position) {
        return boxes.get(position).getIdx();
    }

    public void setSelectionTracker(SelectionTracker<Long> selectionTracker) {
        this.mSelectionTracker = selectionTracker;
    }


    private boolean hasSelection() {
        if (mSelectionTracker != null) {
            return mSelectionTracker.hasSelection();
        }
        return false;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final Box box = boxes.get(position);

        if (holder instanceof BoxViewHolder) {
            BoxViewHolder boxViewHolder = (BoxViewHolder) holder;
            boolean isSelected = false;
            if (mSelectionTracker != null) {
                if (mSelectionTracker.isSelected(box.getIdx())) {
                    isSelected = true;
                }
            }

            boxViewHolder.bind(isSelected, box);

            try {


                String date = getDate(new Date(box.getDate()));
                boxViewHolder.session_date.setText(date);
                int number = box.getNumber();
                if (number > 0) {
                    int color = MaterialColors.getColor(mContext,
                            android.R.attr.colorPrimary, Color.BLACK);
                    TextDrawable left = TextDrawable.builder()
                            .beginConfig()
                            .textColor(Color.WHITE).bold().height(dpToPx()).width(dpToPx())
                            .endConfig()
                            .buildRound("" + number, color);
                    boxViewHolder.session_date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            left, null, null, null);
                    boxViewHolder.session_date.setCompoundDrawablePadding(16);
                } else {
                    boxViewHolder.session_date.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            0, 0, 0, 0);
                    boxViewHolder.session_date.setCompoundDrawablePadding(0);
                }


                String title = MimeTypeService.getCompactString(box.getName());
                boxViewHolder.content_title.setText(title);


                String message = MimeTypeService.getCompactString(box.getContent());
                boxViewHolder.content_subtitle.setText(message);


                int start = 0;

                String mimeType = box.getMimeType();
                if (!MimeType.PLAIN_MIME_TYPE.equals(mimeType)) {
                    start = MimeTypeService.getMediaResource(mimeType);
                }


                boxViewHolder.content_subtitle.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        start, 0, 0, 0);
                boxViewHolder.content_subtitle.setCompoundDrawablePadding(8);
                boxViewHolder.main_image.clearColorFilter();

                if (hasSelection()) {
                    if (isSelected) {
                        boxViewHolder.main_image.setClickable(false);
                        int color = MaterialColors.getColor(mContext,
                                android.R.attr.colorControlHighlight, Color.GRAY);

                        TextDrawable drawable = TextDrawable.builder()
                                .beginConfig()
                                .textColor(Color.WHITE)
                                .endConfig()
                                .buildRound("✓", color);
                        boxViewHolder.main_image.setImageDrawable(drawable);

                    } else {
                        boxViewHolder.main_image.setImageDrawable(
                                MimeTypeService.getNameDrawable(mContext, box.getName()));
                    }
                } else {
                    boxViewHolder.main_image.setImageDrawable(
                            MimeTypeService.getNameDrawable(mContext, box.getName()));

                    boxViewHolder.view.setOnClickListener((v) -> mListener.onClick(box));
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        }


    }


    @NonNull
    private String getDate(@NonNull Date date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        Date today = c.getTime();
        c.set(Calendar.MONTH, 0);
        c.set(Calendar.DAY_OF_MONTH, 0);
        Date lastYear = c.getTime();

        if (date.before(today)) {
            if (date.before(lastYear)) {
                return android.text.format.DateFormat.format("dd.MM.yyyy", date).toString();
            } else {
                return android.text.format.DateFormat.format("dd.MMMM", date).toString();
            }
        } else {
            return android.text.format.DateFormat.format("HH:mm", date).toString();
        }
    }

    private int dpToPx() {
        return (int) (25 * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public int getItemCount() {
        return boxes.size();
    }

    @Override
    public long getItemId(int position) {
        Box box = boxes.get(position);
        return box.getIdx();
    }

    public void updateData(@NonNull List<Box> messageBoxes) {

        final BoxDiffCallback diffCallback = new BoxDiffCallback(this.boxes, messageBoxes);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        this.boxes.clear();
        this.boxes.addAll(messageBoxes);
        diffResult.dispatchUpdatesTo(this);


    }

    @Override
    public synchronized int getPosition(long idx) {
        for (int i = 0; i < boxes.size(); i++) {
            if (boxes.get(i).getIdx() == idx) {
                return i;
            }
        }
        return 0;
    }


    public interface ViewAdapterListener {
        void onClick(@NonNull Box box);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        final View view;
        final TextView content_title;
        final TextView content_subtitle;

        ViewHolder(View v) {
            super(v);
            v.setLongClickable(true);
            v.setFocusable(false);
            v.setClickable(true);
            view = v;
            content_title = v.findViewById(R.id.content_title);
            content_subtitle = v.findViewById(R.id.content_subtitle);
        }
    }


    static class BoxViewHolder extends ViewHolder {
        final ImageView main_image;
        final TextView session_date;
        final BoxItemDetails boxItemDetails;

        BoxViewHolder(BoxItemPosition pos, View v) {
            super(v);
            session_date = v.findViewById(R.id.session_date);
            main_image = v.findViewById(R.id.main_image);
            boxItemDetails = new BoxItemDetails(pos);
        }

        void bind(boolean isSelected, Box thread) {

            boxItemDetails.idx = thread.getIdx();

            itemView.setActivated(isSelected);

        }

        ItemDetailsLookup.ItemDetails<Long> getItemDetails() {

            return boxItemDetails;
        }
    }


}
