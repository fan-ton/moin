package threads.moin.boxes;

import androidx.recyclerview.widget.DiffUtil;

import java.util.List;

import threads.moin.core.boxes.Box;

@SuppressWarnings("WeakerAccess")
public class BoxDiffCallback extends DiffUtil.Callback {
    private final List<Box> mOldList;
    private final List<Box> mNewList;

    public BoxDiffCallback(List<Box> messages, List<Box> messageBoxes) {
        this.mOldList = messages;
        this.mNewList = messageBoxes;
    }

    @Override
    public int getOldListSize() {
        return mOldList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).areItemsTheSame(mNewList.get(
                newItemPosition));
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldList.get(oldItemPosition).sameContent(mNewList.get(newItemPosition));
    }


}
