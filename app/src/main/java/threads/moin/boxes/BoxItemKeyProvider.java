package threads.moin.boxes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.selection.ItemKeyProvider;

public class BoxItemKeyProvider extends ItemKeyProvider<Long> {

    private final BoxViewAdapter mAdapter;


    public BoxItemKeyProvider(@NonNull BoxViewAdapter adapter) {
        super(SCOPE_CACHED);
        mAdapter = adapter;
    }

    @Nullable
    @Override
    public Long getKey(int position) {
        return mAdapter.getIdx(position);
    }

    @Override
    public int getPosition(@NonNull Long key) {
        return mAdapter.getPosition(key);
    }

}