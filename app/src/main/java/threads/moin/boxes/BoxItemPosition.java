package threads.moin.boxes;

interface BoxItemPosition {
    int getPosition(long idx);
}
