package threads.moin;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.SearchView;
import androidx.lifecycle.ViewModelProvider;
import androidx.slidingpanelayout.widget.SlidingPaneLayout;

import com.google.android.material.search.SearchBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textview.MaterialTextView;

import java.util.Base64;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.cid.PeerId;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.PermissionAction;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.events.EVENTS;
import threads.moin.fragments.ProfileDialogFragment;
import threads.moin.model.EventViewModel;
import threads.moin.model.MoinViewModel;
import threads.moin.work.BoxDeleteWorker;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;

    private LinearLayout linearLayout;
    private MoinViewModel moinViewModel;
    private ActionMode actionMode;
    private long lastClickTime = 0;
    private SlidingPaneLayout slidingPaneLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        MaterialTextView offline_mode = findViewById(R.id.offline_mode);
        slidingPaneLayout = findViewById(R.id.sliding_pane_layout);
        getOnBackPressedDispatcher().addCallback(
                this,
                new TwoPaneOnBackPressedCallback(slidingPaneLayout));


        SearchBar searchBar = findViewById(R.id.search_bar);
        linearLayout = findViewById(R.id.main_layout);

        setSupportActionBar(searchBar);

        searchBar.setOnClickListener(v -> ProfileDialogFragment.newInstance().show(
                getSupportFragmentManager(), ProfileDialogFragment.TAG));

        moinViewModel = new ViewModelProvider(this).get(MoinViewModel.class);

        moinViewModel.getBox().observe(this, (box) -> {
            try {
                if (box != null && box > 0) {

                    slidingPaneLayout.open();

                    API.markRead(getApplicationContext(), box);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

        EventViewModel eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);

        eventViewModel.permission().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                moinViewModel.setShowFab(true);
                            }
                        });
                        moinViewModel.setShowFab(false);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.delete().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {

                        String data = content
                                .replace("[", "")
                                .replace("]", "")
                                .trim();

                        String[] parts = data.split(",");

                        long[] idxs = new long[parts.length];
                        for (int i = 0; i < parts.length; i++) {
                            idxs[i] = Long.parseLong(parts[i].trim());
                        }


                        String message;
                        if (idxs.length == 1) {
                            message = getString(R.string.delete_contact);
                        } else {
                            message = getString(R.string.delete_contacts, "" + idxs.length);
                        }
                        AtomicBoolean delete = new AtomicBoolean(true);
                        Snackbar snackbar = Snackbar.make(linearLayout, message, Snackbar.LENGTH_LONG);
                        snackbar.setAction(getString(R.string.revert_operation), (view) -> {
                            try {
                                delete.set(false);
                                BOXES boxes = BOXES.getInstance(getApplicationContext());
                                Executors.newSingleThreadExecutor().execute(() ->
                                        boxes.resetBoxesDeleting(idxs));
                            } catch (Throwable throwable) {
                                LogUtils.error(TAG, throwable);
                            } finally {
                                snackbar.dismiss();
                            }

                        });
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                if (delete.get()) {
                                    BoxDeleteWorker.delete(getApplicationContext(), idxs);
                                }
                                moinViewModel.setShowFab(true);
                            }
                        });
                        moinViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.online().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.GONE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
        eventViewModel.offline().observe(this, (event) -> {
            try {
                if (event != null) {
                    offline_mode.setVisibility(View.VISIBLE);
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });
        eventViewModel.fatal().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());

                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                moinViewModel.setShowFab(true);

                            }
                        });
                        moinViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.error().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (view) -> snackbar.dismiss());
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                moinViewModel.setShowFab(true);
                            }
                        });
                        moinViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        eventViewModel.warning().observe(this, (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {

                        Snackbar snackbar = Snackbar.make(linearLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.addCallback(new Snackbar.Callback() {

                            @Override
                            public void onDismissed(Snackbar snackbar, int event) {
                                moinViewModel.setShowFab(true);
                            }
                        });
                        moinViewModel.setShowFab(false);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        Intent intent = getIntent();
        handleIntents(intent);
    }


    private void handleIntents(Intent intent) {

        final String action = intent.getAction();

        if (Intent.ACTION_VIEW.equals(action)) {
            Uri uri = intent.getData();
            if (uri != null) {

                String scheme = uri.getScheme();
                if (Objects.equals(scheme, Content.SCHEME)) {
                    EVENTS events = EVENTS.getInstance(getApplicationContext());
                    try {
                        PeerId owner = PeerId.decode(uri.getAuthority());
                        String alias = uri.getQueryParameter(Content.ALIAS);
                        if (alias == null || alias.isEmpty()) {
                            alias = API.leftAlias(owner.toString());
                        } else {
                            alias = new String(Base64.getDecoder().decode(alias));
                        }
                        API.createBox(getApplicationContext(), owner, alias);

                    } catch (Throwable throwable) {
                        events.error(getString(
                                R.string.account_identification_not_valid));
                    }
                }
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();

        try {
            if (actionMode != null) {
                actionMode.finish();
                actionMode = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);


        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        SearchView mSearchView = (SearchView) searchMenuItem.getActionView();


        mSearchView.setIconifiedByDefault(true);
        String query = moinViewModel.getQuery().getValue();
        Objects.requireNonNull(query);
        mSearchView.setQuery(query, true);
        mSearchView.setIconified(query.isEmpty());


        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                moinViewModel.setQuery(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                moinViewModel.setQuery(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        networkCallback();
    }

    public void networkCallback() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            EVENTS events = EVENTS.getInstance(getApplicationContext());
            ConnectivityManager.NetworkCallback networkCallback =
                    new ConnectivityManager.NetworkCallback() {
                        @Override
                        public void onAvailable(Network network) {
                            super.onAvailable(network);
                            events.online();
                        }

                        @Override
                        public void onLost(Network network) {
                            super.onLost(network);
                            events.offline();
                        }

                    };

            NetworkRequest networkRequest = new NetworkRequest.Builder()
                    .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                    .build();

            connectivityManager.registerNetworkCallback(networkRequest, networkCallback);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();


        if (item.getItemId() == android.R.id.home) {

            try {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                ProfileDialogFragment.newInstance().show(
                        getSupportFragmentManager(), ProfileDialogFragment.TAG);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
            return true;
        } else if (id == R.id.action_search) {

            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            return true;

        } else if (item.getItemId() == R.id.action_doc) {

            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return true;
            }

            lastClickTime = SystemClock.elapsedRealtime();

            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://gitlab.com/remmer.wilts/moin"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            } catch (Throwable e) {
                EVENTS.getInstance(getApplicationContext()).warning(
                        getString(R.string.no_activity_found_to_handle_uri));
            }

            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    class TwoPaneOnBackPressedCallback extends OnBackPressedCallback
            implements SlidingPaneLayout.PanelSlideListener {

        private final SlidingPaneLayout mSlidingPaneLayout;

        TwoPaneOnBackPressedCallback(@NonNull SlidingPaneLayout slidingPaneLayout) {
            // Set the default 'enabled' state to true only if it is slide able (i.e., the panes
            // are overlapping) and open (i.e., the detail pane is visible).
            super(slidingPaneLayout.isSlideable() && slidingPaneLayout.isOpen());
            mSlidingPaneLayout = slidingPaneLayout;
            slidingPaneLayout.addPanelSlideListener(this);
        }

        @Override
        public void handleOnBackPressed() {
            // Return to the list pane when the system back button is pressed.
            mSlidingPaneLayout.closePane();
        }

        @Override
        public void onPanelSlide(@NonNull View panel, float slideOffset) {
        }

        @Override
        public void onPanelOpened(@NonNull View panel) {
            // Intercept the system back button when the detail pane becomes visible.
            setEnabled(true);

            invalidateOptionsMenu();
        }

        @Override
        public void onPanelClosed(@NonNull View panel) {
            // Disable intercepting the system back button when the user returns to the
            // list pane.
            setEnabled(false);

            invalidateOptionsMenu();
        }
    }

}
