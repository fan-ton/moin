package threads.moin;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import com.google.android.material.color.DynamicColors;

import java.util.Objects;

import threads.lite.IPFS;
import threads.lite.core.Server;
import threads.moin.core.API;
import threads.moin.core.events.EVENTS;
import threads.moin.services.DaemonService;
import threads.moin.services.DiscoveryService;
import threads.moin.services.RegistrationService;
import threads.moin.services.RequestService;
import threads.moin.work.CleanupWorker;

public class InitApplication extends Application {

    public static final String SERVICE = "_moin._udp";
    public static final String MESSAGE_CHANNEL_ID = "MESSAGE_CHANNEL_ID";
    public static final String DAEMON_CHANNEL_ID = "DAEMON_CHANNEL_ID";
    private static final String TAG = InitApplication.class.getSimpleName();


    private NsdManager nsdManager;
    private DiscoveryService discoveryService;

    private void createMessageChannel(@NonNull Context context) {

        try {

            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);
            CharSequence name = context.getString(R.string.message_channel_name);
            String description = context.getString(R.string.message_channel_description);

            NotificationChannel mChannel = new NotificationChannel(
                    MESSAGE_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.setDescription(description);

            notificationManager.createNotificationChannel(mChannel);


        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private void registerService() {
        EVENTS events = EVENTS.getInstance(getApplicationContext());
        try {
            API api = API.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            ipfs.setIncomingPush(litePush -> RequestService.evaluate(
                    getApplicationContext(), litePush));

            Server server = api.getServer();
            String peerID = ipfs.self().toString();

            NsdServiceInfo serviceInfo = new NsdServiceInfo();
            serviceInfo.setServiceName(peerID);
            serviceInfo.setServiceType(InitApplication.SERVICE);
            serviceInfo.setPort(server.getPort());

            nsdManager = (NsdManager) getSystemService(Context.NSD_SERVICE);
            Objects.requireNonNull(nsdManager);
            discoveryService = new DiscoveryService(api.getSession(), nsdManager);
            nsdManager.discoverServices(InitApplication.SERVICE, NsdManager.PROTOCOL_DNS_SD,
                    discoveryService);
            nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD,
                    RegistrationService.getInstance());


        } catch (Throwable throwable) {
            events.fatal(
                    throwable.getClass().getSimpleName() +
                            " " + throwable.getMessage());
            unregisterService();
            LogUtils.error(TAG, throwable);
        }
    }

    private void createDaemonChannel(@NonNull Context context) {

        try {
            CharSequence name = context.getString(R.string.daemon_channel_name);
            String description = context.getString(R.string.daemon_channel_description);
            NotificationChannel mChannel = new NotificationChannel(DAEMON_CHANNEL_ID, name,
                    NotificationManager.IMPORTANCE_LOW);

            mChannel.setDescription(description);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);

            notificationManager.createNotificationChannel(mChannel);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    private void unregisterService() {
        try {
            if (nsdManager != null) {
                nsdManager.unregisterService(RegistrationService.getInstance());
                if (discoveryService != null) {
                    nsdManager.stopServiceDiscovery(discoveryService);
                    discoveryService = null;
                }
                nsdManager = null;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unregisterService();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        DynamicColors.applyToActivitiesIfAvailable(this);

        createDaemonChannel(getApplicationContext());
        createMessageChannel(getApplicationContext());

        registerService();

        CleanupWorker.cleanup(getApplicationContext());

        DaemonService.start(getApplicationContext());
    }

}

