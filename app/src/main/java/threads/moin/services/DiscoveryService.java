package threads.moin.services;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import androidx.annotation.NonNull;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import threads.lite.IPFS;
import threads.lite.cid.Multiaddr;
import threads.lite.cid.PeerId;
import threads.lite.core.Parameters;
import threads.lite.core.Session;
import threads.moin.LogUtils;

public class DiscoveryService implements NsdManager.DiscoveryListener {
    private static final String TAG = DiscoveryService.class.getSimpleName();
    private final Session session;
    private final NsdManager nsdManager;

    public DiscoveryService(@NonNull Session session, @NonNull NsdManager nsdManager) {
        this.session = session;
        this.nsdManager = nsdManager;
    }

    @Override
    public void onStartDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.error(TAG, "onStartDiscoveryFailed");
    }

    @Override
    public void onStopDiscoveryFailed(String serviceType, int errorCode) {
        LogUtils.error(TAG, "onStopDiscoveryFailed");
    }

    @Override
    public void onDiscoveryStarted(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStarted");
    }

    @Override
    public void onDiscoveryStopped(String serviceType) {
        LogUtils.debug(TAG, "onDiscoveryStopped");
    }

    @Override
    public void onServiceFound(NsdServiceInfo serviceInfo) {
        nsdManager.resolveService(serviceInfo, new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo nsdServiceInfo, int i) {
                LogUtils.error(TAG, "onResolveFailed " + nsdServiceInfo.toString());
            }

            @Override
            public void onServiceResolved(NsdServiceInfo nsdServiceInfo) {
                try {
                    LogUtils.error(TAG, "onResolveResolved " + nsdServiceInfo.toString());
                    evaluate(nsdServiceInfo);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }
            }
        });
    }

    @Override
    public void onServiceLost(NsdServiceInfo serviceInfo) {
        LogUtils.error(TAG, "onServiceLost " + serviceInfo.getServiceName());
    }


    public void evaluate(@NonNull NsdServiceInfo serviceInfo) {
        try {
            PeerId decodedPeerId = PeerId.decode(serviceInfo.getServiceName());
            InetAddress inetAddress = serviceInfo.getHost();
            Multiaddr multiaddr = Multiaddr.create(decodedPeerId,
                    new InetSocketAddress(inetAddress, serviceInfo.getPort()));
            session.connect(multiaddr, Parameters.getDefault(
                    IPFS.GRACE_PERIOD_RESERVATION, true));
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

}
