package threads.moin.services;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

import threads.lite.IPFS;
import threads.lite.cbor.CborObject;
import threads.lite.cbor.Cborable;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Connection;
import threads.lite.core.IpnsEntity;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Session;
import threads.lite.host.LitePush;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.FcmContent;
import threads.moin.core.MimeType;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.work.DownloadFileWorker;
import threads.moin.work.UnpinWorker;


public class RequestService {
    private static final String TAG = RequestService.class.getSimpleName();
    private static final Set<Long> identities = ConcurrentHashMap.newKeySet();

    public static void evaluate(@NonNull Context context, @NonNull LitePush push) {
        Executors.newSingleThreadExecutor().execute(() -> {
            try {
                API api = API.getInstance(context);
                IpnsEntity ipnsEntity = push.getIpnsEntity();

                PeerId owner = ipnsEntity.getPeerId();
                byte[] value = ipnsEntity.getValue();
                Connection connection = push.getConnection();


                CborObject deserialized = CborObject.fromByteArray(value);

                Objects.requireNonNull(deserialized);
                CborObject.CborMap map = (CborObject.CborMap) deserialized;
                Objects.requireNonNull(map);
                SortedMap<CborObject, ? extends Cborable> data = map.values;

                String est = ((CborObject.CborString) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.EST)))).value;
                Objects.requireNonNull(est);
                if (FcmContent.RECEIVED.name().equals(est)) {
                    long ident = ((CborObject.CborLong) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.IDENT)))).value;

                    MAILS mails = MAILS.getInstance(context);
                    try {
                        mails.setMailReceived(ident);
                        UnpinWorker.unpin(context, ident);
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                } else if (FcmContent.LEAVE.name().equals(est)) {
                    long timestamp = ((CborObject.CborLong) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.TIME)))).value;
                    RequestService.leave(context, owner, timestamp);
                } else if (FcmContent.MESSAGE.name().equals(est)) {
                    String text = ((CborObject.CborString) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.TXT)))).value;
                    Objects.requireNonNull(text);
                    long timestamp = ((CborObject.CborLong) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.TIME)))).value;
                    long ident = ((CborObject.CborLong) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.IDENT)))).value;

                    // check if push message was not send before
                    if (identities.add(ident)) {
                        boolean success =
                                RequestService.message(context, owner, text, timestamp);
                        if (success) {
                            api.received(connection, ident);
                        }
                    }
                } else {
                    long ident = ((CborObject.CborLong) Objects.requireNonNull(
                            data.get(new CborObject.CborString(Content.IDENT)))).value;

                    // check if push message was not send before
                    if (identities.add(ident)) {
                        RequestService.data(context, owner, ident, data);
                    }
                }


            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

    private static boolean message(@NonNull Context context, @NonNull PeerId owner,
                                   @NonNull String text, long timestamp) {

        try {
            MAILS mails = MAILS.getInstance(context);
            BOXES boxes = BOXES.getInstance(context);
            Box box = boxes.getBoxByOwner(owner);
            if (box != null) {
                Mail mail = mails.createMessageMail(box.getIdx(), owner, text, timestamp);
                mail.setMimeType(MimeType.PLAIN_MIME_TYPE);
                mail.setLeaching(false);
                mail.setSeeding(true);

                long idx = mails.storeMail(mail);

                NotificationService.notification(context, idx);

                return true;

            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    private static void data(@NonNull Context context, @NonNull PeerId owner,
                             long ident, @NonNull SortedMap<CborObject, ? extends Cborable> data) {

        try {
            BOXES boxes = BOXES.getInstance(context);
            MAILS mails = MAILS.getInstance(context);

            Box box = boxes.getBoxByOwner(owner);
            if (box != null) {
                String estType = ((CborObject.CborString) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.EST)))).value;
                Objects.requireNonNull(estType);

                Mail.Type type = Mail.Type.valueOf(estType);

                String content = ((CborObject.CborString) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.CID)))).value;
                Objects.requireNonNull(content);
                Cid cid = Cid.decode(content);

                String mimeType = ((CborObject.CborString) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.MIME_TYPE)))).value;
                Objects.requireNonNull(mimeType);


                long fileSize = ((CborObject.CborLong) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.FILESIZE)))).value;

                String text = null;
                Cborable txt = data.get(new CborObject.CborString(Content.TXT));
                if (txt != null) {
                    text = ((CborObject.CborString) txt).value;
                }

                String filename = ((CborObject.CborString) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.FILENAME)))).value;
                Objects.requireNonNull(filename);

                long timestamp = ((CborObject.CborLong) Objects.requireNonNull(
                        data.get(new CborObject.CborString(Content.TIME)))).value;

                Mail mail = Mail.createMail(box.getIdx(), owner, type, timestamp);
                if (text != null) {
                    mail.setText(text);
                }
                mail.setIdent(ident);
                mail.setMimeType(mimeType);
                mail.setSize(fileSize);
                mail.setFilename(filename);
                mail.setCid(cid);

                if (mail.getType() == Mail.Type.FILE) {
                    mail.setText(filename);
                }

                mail.setLeaching(false);
                long idx = mails.storeMail(mail);

                NotificationService.notification(context, idx);


                if (mail.getType() == Mail.Type.AUDIO || mail.getType() == Mail.Type.FILE) {
                    DownloadFileWorker.download(context, idx);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    private static void leave(@NonNull Context context, @NonNull PeerId owner,
                              long timestamp) {

        try {
            BOXES boxes = BOXES.getInstance(context);
            MAILS mails = MAILS.getInstance(context);

            Box box = boxes.getBoxByOwner(owner);
            if (box != null) {
                String message = context.getString(R.string.user_leaves, box.getName());
                long idx = mails.createInfo(box.getIdx(), owner, message, timestamp);
                NotificationService.notification(context, idx);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


    public static void incomingConnection(@NonNull Context context, @NonNull Connection connection) {


        Executors.newSingleThreadExecutor().execute(() -> {
            long start = System.currentTimeMillis();

            try {
                API api = API.getInstance(context);
                IPFS ipfs = IPFS.getInstance(context);
                MAILS mails = MAILS.getInstance(context);
                BOXES boxes = BOXES.getInstance(context);

                Box box = boxes.getBoxByOwner(connection.remotePeerId());
                if (box == null) {
                    return;
                }

                // adding a connection here [has to be tested]
                Session session = api.getSession();
                Connection remoteConn = session.getConnection(connection.remotePeerId());
                if (remoteConn == null) {
                    session.addSwarmConnection(connection);
                }

                // push all stored notifications
                List<Mail> notifications = mails.getNotifications(ipfs.self(), box.getIdx());
                for (Mail notification : notifications) {

                    byte[] push = Mail.pushFormat(notification);

                    IpnsRecord ipnsRecord = ipfs.createSelfSignedIpnsRecord(
                            0, push, null);

                    ipfs.push(connection, ipnsRecord).whenComplete((unused, throwable) -> {
                        if (throwable == null) {
                            long idx = notification.getIdx();
                            if (idx >= 0) {
                                mails.setMailPublished(idx);
                            }
                        } else {
                            LogUtils.error(TAG, throwable);
                        }
                    });
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }
        });
    }

}
