package threads.moin.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;

import androidx.annotation.NonNull;

import threads.lite.cid.IPV;
import threads.moin.R;
import threads.moin.core.ColorGenerator;
import threads.moin.core.MimeType;
import threads.moin.core.TextDrawable;

public class MimeTypeService {

    private static final int FONT_SIZE = 25;

    public static String getCompactString(@NonNull String title) {

        return title.replace("\n", " ");
    }

    @NonNull
    public static Bitmap getPortBitmap(@NonNull Context context, long port) {

        Canvas canvas = new Canvas();
        int color = context.getColor(android.R.color.holo_blue_dark);
        int textColor = context.getColor(android.R.color.white);
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig().textColor(textColor).bold().fontSize(FONT_SIZE).endConfig()
                .buildRoundRect("Port " + port, color, 8);
        Bitmap bitmap = Bitmap.createBitmap(FONT_SIZE * 6, FONT_SIZE * 2,
                Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, FONT_SIZE * 6, FONT_SIZE * 2);

        drawable.draw(canvas);
        return bitmap;
    }

    @NonNull
    public static Bitmap getIPvBitmap(@NonNull Context context, @NonNull IPV ipv) {

        Canvas canvas = new Canvas();
        int color = context.getColor(android.R.color.holo_blue_dark);
        int textColor = context.getColor(android.R.color.white);
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig().textColor(textColor).bold().fontSize(FONT_SIZE).endConfig()
                .buildRoundRect(ipv.name(), color, 8);
        Bitmap bitmap = Bitmap.createBitmap(FONT_SIZE * 6, FONT_SIZE * 2,
                Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, FONT_SIZE * 6, FONT_SIZE * 2);

        drawable.draw(canvas);
        return bitmap;
    }


    @NonNull
    public static Bitmap getNameImage(@NonNull Context context, @NonNull String name) {
        return getNameImage(context, name, 128);
    }

    @NonNull
    public static Bitmap getNameImage(@NonNull Context context, @NonNull String name, int size) {
        Canvas canvas = new Canvas();
        TextDrawable drawable = getNameDrawable(context, name);
        Bitmap bitmap = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, size, size);

        drawable.draw(canvas);
        return bitmap;
    }

    @NonNull
    public static TextDrawable getNameDrawable(@NonNull Context context, @NonNull String name) {

        if (name.isEmpty()) {
            name = "UNKNOWN";
        }
        String letter = name.substring(0, 1);
        int color = ColorGenerator.MATERIAL.getColor(name);
        int textColor = context.getColor(android.R.color.white);
        return TextDrawable.builder()
                .beginConfig()
                .textColor(textColor)
                .endConfig()
                .buildRound(letter, color);
    }

    public static int getMediaResource(@NonNull String mimeType) {

        if (!mimeType.isEmpty()) {
            if (mimeType.equals(MimeType.TORRENT_MIME_TYPE)) {
                return R.drawable.arrow_up_down_bold;
            }
            if (mimeType.equals(MimeType.OCTET_MIME_TYPE)) {
                return R.drawable.file_star;
            }
            if (mimeType.equals(MimeType.JSON_MIME_TYPE)) {
                return R.drawable.json;
            }
            if (mimeType.equals(MimeType.PLAIN_MIME_TYPE)) {
                return R.drawable.file;
            }
            if (mimeType.equals(MimeType.PDF_MIME_TYPE)) {
                return R.drawable.pdf;
            }
            if (mimeType.equals(MimeType.WORD_MIME_TYPE)) {
                return R.drawable.microsoft_word;
            }
            if (mimeType.equals(MimeType.EXCEL_MIME_TYPE)
                    || mimeType.equals(MimeType.CSV_MIME_TYPE)
                    || mimeType.equals(MimeType.OPEN_EXCEL_MIME_TYPE)) {
                return R.drawable.microsoft_excel;
            }
            if (mimeType.startsWith(MimeType.TEXT)) {
                return R.drawable.file;
            }
            if (mimeType.equals(MimeType.DIR_MIME_TYPE)) {
                return R.drawable.folder;
            }
            if (mimeType.startsWith(MimeType.VIDEO)) {
                return R.drawable.movie_outline;
            }
            if (mimeType.startsWith(MimeType.IMAGE)) {
                return R.drawable.camera;
            }
            if (mimeType.startsWith(MimeType.AUDIO)) {
                return R.drawable.audio;
            }
            if (mimeType.startsWith(MimeType.PGP_KEYS_MIME_TYPE)) {
                return R.drawable.fingerprint;
            }
            if (mimeType.startsWith(MimeType.CONTACT)) {
                return R.drawable.card;
            }
            if (mimeType.startsWith(MimeType.APPLICATION)) {
                return R.drawable.application;
            }

            return R.drawable.settings;
        }

        return R.drawable.help;

    }

}
