package threads.moin.services;

import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;

import threads.moin.LogUtils;

public class RegistrationService implements NsdManager.RegistrationListener {
    private static final String TAG = RegistrationService.class.getSimpleName();

    public static RegistrationService getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        LogUtils.verbose(TAG, "RegistrationFailed : " + errorCode);
    }

    @Override
    public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {
        LogUtils.verbose(TAG, "Un-RegistrationFailed : " + errorCode);
    }

    @Override
    public void onServiceRegistered(NsdServiceInfo serviceInfo) {
        LogUtils.verbose(TAG, "ServiceRegistered : " + serviceInfo.getServiceName());
    }

    @Override
    public void onServiceUnregistered(NsdServiceInfo serviceInfo) {
        LogUtils.verbose(TAG, "Un-ServiceRegistered : " + serviceInfo.getServiceName());
    }

    private static final class InstanceHolder {
        private static final RegistrationService INSTANCE = new RegistrationService();
    }
}
