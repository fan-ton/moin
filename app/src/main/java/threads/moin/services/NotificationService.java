package threads.moin.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import androidx.annotation.NonNull;

import java.util.Objects;

import threads.moin.InitApplication;
import threads.moin.LogUtils;
import threads.moin.MainActivity;
import threads.moin.R;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;

public class NotificationService {
    private static final String TAG = NotificationService.class.getSimpleName();

    private static String getCompactString(@NonNull String title) {
        return title.replace("\n", " ");
    }

    public static void notification(@NonNull Context context, long idx) {

        long start = System.currentTimeMillis();
        try {
            BOXES boxes = BOXES.getInstance(context);
            MAILS mails = MAILS.getInstance(context);
            showNotification(context, idx);

            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);

            String mimeType = mail.getMimeType();

            String text = mail.getText();
            if (text.isEmpty()) {
                if (mail.getType() == Mail.Type.AUDIO) {
                    text = context.getString(R.string.audio);
                } else if (mail.getType() == Mail.Type.MESSAGE) {
                    text = context.getString(R.string.message);
                } else {
                    text = "";
                }
            }

            boxes.setBoxContent(mail.getBox(), text, mimeType);
            boxes.incrementBoxesNumber(mail.getBox());

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

    }

    private static void showNotification(@NonNull Context context, long idx) {
        try {
            NotificationManager notificationManager = (NotificationManager)
                    context.getSystemService(Context.NOTIFICATION_SERVICE);

            MAILS mails = MAILS.getInstance(context);
            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);


            String text = mail.getText();
            if (text.isEmpty()) {
                if (mail.getType() == Mail.Type.AUDIO) {
                    text = context.getString(R.string.audio);
                } else if (mail.getType() == Mail.Type.MESSAGE) {
                    text = context.getString(R.string.message);
                } else {
                    text = "";
                }
            }

            BOXES boxes = BOXES.getInstance(context);
            Box box = boxes.getBox(mail);
            Objects.requireNonNull(box);
            String alias = box.getName();

            Bitmap bitmap = MimeTypeService.getNameImage(context, alias);

            Intent notifyIntent = new Intent(context, MainActivity.class);
            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestID,
                    notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT |
                            PendingIntent.FLAG_IMMUTABLE);

            Notification.Builder builder = new Notification.Builder(context,
                    InitApplication.MESSAGE_CHANNEL_ID);

            builder.setContentTitle(getCompactString(alias))
                    .setContentIntent(pendingIntent)
                    .setContentText(getCompactString(text))
                    .setLargeIcon(bitmap)
                    .setAutoCancel(true)
                    .setCategory(Notification.CATEGORY_MESSAGE)
                    .setSmallIcon(R.drawable.forum_outline);

            Notification notification = builder.build();


            notificationManager.notify((int) box.getIdx(), notification);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }


}

