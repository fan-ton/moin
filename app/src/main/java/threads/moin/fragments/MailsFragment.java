package threads.moin.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

import threads.lite.cid.PeerId;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.MimeType;
import threads.moin.core.PermissionAction;
import threads.moin.core.events.EVENTS;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.mails.MailAdapterListener;
import threads.moin.mails.MailViewAdapter;
import threads.moin.model.EventViewModel;
import threads.moin.model.MoinViewModel;
import threads.moin.provider.Provider;
import threads.moin.work.DownloadFileWorker;
import threads.moin.work.MailExportWorker;
import threads.moin.work.UploadFileWorker;

public class MailsFragment extends Fragment implements MailAdapterListener {

    private static final String TAG = MailsFragment.class.getSimpleName();

    private static final int CLICK_OFFSET = 500;
    private final AtomicReference<String> selfName = new AtomicReference<>("");
    private final AtomicReference<PeerId> self = new AtomicReference<>(null);
    @NonNull
    private final MediaPlayer mediaPlayer = new MediaPlayer();
    private MailViewAdapter mailViewAdapter;
    private BottomNavigationView bottomNavigationView;
    private MoinViewModel moinViewModel;
    private final ActivityResultLauncher<Intent> mExportFileForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);
                        Uri uri = data.getData();
                        Objects.requireNonNull(uri);

                        if (!Provider.hasWritePermission(requireContext(), uri)) {
                            EVENTS.getInstance(requireContext()).error(
                                    getString(R.string.file_has_no_write_permission));
                            return;
                        }

                        MailExportWorker.export(requireContext(), uri, getNote());
                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });
    private final ActivityResultLauncher<Intent> mSelectFilesForResult = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    try {
                        Objects.requireNonNull(data);
                        if (data.getClipData() != null) {
                            ClipData mClipData = data.getClipData();
                            API.files(requireContext(), mClipData, getBox());

                        } else if (data.getData() != null) {
                            Uri uri = data.getData();

                            if (!Provider.hasReadPermission(requireContext(), uri)) {
                                EVENTS.getInstance(requireContext()).error(
                                        getString(R.string.file_has_no_read_permission));
                                return;
                            }

                            if (Provider.isPartial(requireContext(), uri)) {
                                EVENTS.getInstance(requireContext()).error(
                                        getString(R.string.file_not_valid));
                                return;
                            }

                            API.uploadFile(requireContext(), uri, getBox());
                        }

                    } catch (Throwable throwable) {
                        LogUtils.error(TAG, throwable);
                    }
                }
            });


    private final ActivityResultLauncher<String> mAudioPermissionResult = registerForActivityResult(
            new ActivityResultContracts.RequestPermission(),
            result -> {
                if (result) {
                    dispatchTakeAudioIntent();
                } else {
                    EVENTS.getInstance(requireContext()).permission(
                            getString(R.string.permission_audio_denied));
                }
            });
    private long lastClickTime = 0;

    @Override
    public void onPause() {
        super.onPause();
        try {
            stopAudioHolder();
            audioHolder.set(null);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickMenuUpload() {
        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType(MimeType.ALL);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.putExtra(DocumentsContract.EXTRA_EXCLUDE_SELF, false);

            mSelectFilesForResult.launch(intent);

        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }
    }

    private void clickMenuMicro() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            mAudioPermissionResult.launch(Manifest.permission.RECORD_AUDIO);
        } else {
            dispatchTakeAudioIntent();
        }
    }


    private void dispatchTakeAudioIntent() {
        try {
            VoiceDialogFragment.newInstance(getBox()).show(
                    getChildFragmentManager(), VoiceDialogFragment.TAG);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void clickMenuMessage() {

        MessageDialogFragment dialogFragment = new MessageDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Content.BOX, getBox());
        dialogFragment.setArguments(bundle);

        dialogFragment.show(getChildFragmentManager(), MessageDialogFragment.TAG);
    }


    @Override
    public void invokeCopyTo(@NonNull Mail mail) {

        try {
            String name = mail.getFilename();

            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType(mail.getMimeType());
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            intent.putExtra(Intent.EXTRA_TITLE, name);

            setNote(mail.getIdx());
            mExportFileForResult.launch(intent);
        } catch (Throwable e) {
            EVENTS.getInstance(requireContext()).warning(
                    getString(R.string.no_activity_found_to_handle_uri));
        }


    }

    @Override
    public void invokeAction(@NonNull Mail mail) {

        try {
            if (!mail.isSeeding()) {
                return;
            }

            if (mail.getType() == Mail.Type.INFO) {
                return;
            }

            if (mail.getType() == Mail.Type.MESSAGE) {
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());
                builder.setTitle(getString(R.string.message));
                builder.setMessage(mail.getText());
                builder.setCancelable(true);

                builder.setPositiveButton(
                        android.R.string.ok,
                        (dialog, id) -> dialog.cancel());

                builder.create().show();
                return;
            }


            try {
                Uri uri = Uri.parse(mail.getUri());
                String mimeType = mail.getMimeType();
                String name = mail.getFilename();

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra(Intent.EXTRA_TITLE, name);
                intent.setDataAndType(uri, mimeType);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                startActivity(intent);
            } catch (Throwable ignore) {
                EVENTS.getInstance(requireContext()).warning(
                        getString(R.string.no_activity_found_to_handle_uri));
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

    }

    @Override
    public void invokeDownloadAction(@NonNull Mail mail) {
        DownloadFileWorker.download(requireContext(), mail.getIdx());
    }

    @Override
    public void invokePauseAction(@NonNull Mail mail) {

        MAILS mails = MAILS.getInstance(requireContext());

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() ->
                mails.resetLeaching(mail.getIdx()));

        UUID uuid = mail.getWorkUUID();
        if (uuid != null) {
            WorkManager.getInstance(requireContext()).cancelWorkById(uuid);
        }

    }

    @Override
    public void invokeUploadAction(@NonNull Mail mail) {
        UploadFileWorker.upload(requireContext(), mail.getIdx());
    }

    @Override
    public String getAlias() {
        return moinViewModel.getAliasValue();
    }

    @Override
    public PeerId self() {
        return self.get();
    }

    @Override
    public String selfName() {
        return selfName.get();
    }

    @NonNull
    @Override
    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.mails_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view_messages);


        selfName.set(API.getAlias(requireContext()));

        try {
            self.set(API.getInstance(requireContext()).self());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        LinearLayout mailsLayout = view.findViewById(R.id.mails_layout);

        PackageManager pm = requireContext().getPackageManager();

        boolean hasMic = pm.hasSystemFeature(PackageManager.FEATURE_MICROPHONE);

        bottomNavigationView = view.findViewById(R.id.navigation);
        if (!hasMic) {
            bottomNavigationView.findViewById(R.id.navigation_micro).setVisibility(View.GONE);
        }

        bottomNavigationView.refreshDrawableState();
        bottomNavigationView.setOnItemSelectedListener((item) -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return true;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            try {
                int itemId = item.getItemId();
                if (itemId == R.id.navigation_message) {
                    clickMenuMessage();
                    return true;
                } else if (itemId == R.id.navigation_micro) {
                    clickMenuMicro();
                    return true;
                } else if (itemId == R.id.navigation_files) {
                    clickMenuUpload();
                    return true;
                } else {
                    return false;
                }
            } finally {
                stopAudioHolder();
            }

        });


        EventViewModel eventViewModel = new ViewModelProvider(this).get(EventViewModel.class);


        eventViewModel.error().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mailsLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(android.R.string.ok, (v) -> snackbar.dismiss());
                        snackbar.setAnchorView(bottomNavigationView);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);

                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.warning().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mailsLayout, content,
                                Snackbar.LENGTH_SHORT);
                        snackbar.setAnchorView(bottomNavigationView);
                        snackbar.show();
                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });


        eventViewModel.permission().observe(getViewLifecycleOwner(), (event) -> {
            try {
                if (event != null) {
                    String content = event.getContent();
                    if (!content.isEmpty()) {
                        Snackbar snackbar = Snackbar.make(mailsLayout, content,
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction(R.string.app_settings, new PermissionAction());
                        snackbar.setAnchorView(bottomNavigationView);
                        snackbar.show();

                    }
                    eventViewModel.removeEvent(event);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }

        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        mailViewAdapter = new MailViewAdapter(requireContext(), this);

        recyclerView.setAdapter(mailViewAdapter);


        moinViewModel = new ViewModelProvider(requireActivity()).get(MoinViewModel.class);

        moinViewModel.getMails().observe(getViewLifecycleOwner(), mails -> {
            try {
                if (mails != null) {
                    int before = mailViewAdapter.getItemCount();
                    mailViewAdapter.updateData(mails);

                    if (before != mails.size()) {
                        recyclerView.scrollToPosition(0);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

    }

    private long getNote() {
        return moinViewModel.getNoteValue();
    }

    private void setNote(long idx) {
        moinViewModel.setNote(idx);
    }

    public long getBox() {
        return moinViewModel.getBoxValue();
    }
}
