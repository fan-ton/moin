package threads.moin.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.Objects;

import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.events.EVENTS;

@SuppressWarnings("WeakerAccess")
public class MessageDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = MessageDialogFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;

    private long mLastClickTime = 0;
    private TextView mChatBox;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.getBehavior().setState(BottomSheetBehavior.STATE_EXPANDED);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.message_view);

        mChatBox = dialog.findViewById(R.id.chat_box);
        Objects.requireNonNull(mChatBox);
        TextView sendAction = dialog.findViewById(R.id.chat_action);
        Objects.requireNonNull(sendAction);
        TextView abortAction = dialog.findViewById(R.id.abort_action);
        Objects.requireNonNull(abortAction);


        Bundle bundle = getArguments();
        Objects.requireNonNull(bundle);

        long box = bundle.getLong(Content.BOX);


        mChatBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                sendAction.setEnabled(s.length() > 0);
            }
        });


        abortAction.setOnClickListener((v) -> dismiss());

        sendAction.setEnabled(false);
        sendAction.setOnClickListener((v) -> {
            try {
                if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();

                removeKeyboards();

                CharSequence text = mChatBox.getText();
                String content = "";
                if (text != null) {
                    content = text.toString();
                    content = content.trim();
                }

                if (!content.isEmpty()) {
                    API.storeText(requireContext(), box, content);
                } else {
                    EVENTS.getInstance(requireContext()).error(
                            getString(R.string.message_empty));
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            } finally {
                dismiss();
            }
        });


        if (mChatBox.requestFocus()) {
            try {

                Window window = dialog.getWindow();
                if (window != null) {
                    window.setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }

        return dialog;
    }

    private void removeKeyboards() {
        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mChatBox.getWindowToken(), 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


}
