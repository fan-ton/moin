package threads.moin.fragments;


import android.app.Dialog;
import android.content.Context;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.provider.Provider;

public class VoiceDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = VoiceDialogFragment.class.getSimpleName();
    private static final int CLICK_OFFSET = 500;
    private final AtomicBoolean record = new AtomicBoolean(false);
    private Chronometer mChronometer;
    private ImageView image_action;
    private MediaRecorder mRecorder;
    private long box;
    private File mOutputFile;
    private long mLastClickTime = 0;

    public static VoiceDialogFragment newInstance(long box) {

        Bundle bundle = new Bundle();
        bundle.putLong(Content.IDX, box);

        VoiceDialogFragment fragment = new VoiceDialogFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @NonNull
    private static File getDataFile(@NonNull Context context, @NonNull String name) {
        Provider provider = Provider.getInstance(context);
        File file = new File(provider.getDataDir(), name);
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) {
                    throw new RuntimeException("File couldn't be created.");
                }
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        }
        return file;
    }


    private boolean startRecording() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            mRecorder = new MediaRecorder(requireContext());
        } else {
            mRecorder = new MediaRecorder();
        }
        mRecorder.setAudioChannels(2);
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.HE_AAC);
        mRecorder.setAudioEncodingBitRate(48000);
        mRecorder.setAudioSamplingRate(16000);
        mRecorder.setOutputFile(mOutputFile.getAbsolutePath());

        try {
            mRecorder.prepare();
            mRecorder.start();
            mChronometer.setBase(SystemClock.elapsedRealtime());
            mChronometer.start();

            return true;
        } catch (Throwable e) {
            return false;
        }
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle args = getArguments();
        Objects.requireNonNull(args);
        box = args.getLong(Content.IDX);

        LayoutInflater inflater = LayoutInflater.from(requireContext());
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(requireContext());


        View view = inflater.inflate(R.layout.audio_recording, null);


        String name = getString(R.string.voice_recording) + "_" + (int) (Math.random() * 10000 + 1);
        final String extension = "m4a";
        String filename = name + "." + extension;
        this.mOutputFile = getDataFile(requireContext(), filename);
        this.mChronometer = view.findViewById(R.id.timer);
        this.image_action = view.findViewById(R.id.image_action);
        this.image_action.setOnClickListener(this);
        this.image_action.setClickable(true);

        builder.setView(view)
                // Add action buttons
                .setPositiveButton(android.R.string.ok, (dialog, id) -> {

                    if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    stopRecording();

                    if (mOutputFile.length() > 0) {
                        API.storeAudio(requireContext(), box, mOutputFile);
                    }
                    dismiss();

                })
                .setNeutralButton(android.R.string.cancel, (dialog, id) -> {

                    // mis-clicking prevention, using threshold of 1000 ms
                    if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
                        return;
                    }

                    mLastClickTime = SystemClock.elapsedRealtime();

                    stopRecording();

                    dismiss();

                });


        AlertDialog dialog = builder.create();

        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private void stopRecording() {
        try {
            if (mRecorder != null) {
                mRecorder.stop();
                mRecorder.release();
                mRecorder = null;
                mChronometer.stop();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onClick(View view) {

        if (SystemClock.elapsedRealtime() - mLastClickTime < CLICK_OFFSET) {
            return;
        }

        mLastClickTime = SystemClock.elapsedRealtime();


        if (!record.get()) {
            if (!startRecording()) {
                image_action.setEnabled(false);
            } else {
                image_action.setImageResource(R.drawable.accent_stop_circle);
                record.set(true);
            }
        } else {
            stopRecording();
            image_action.setImageResource(R.drawable.record);
            record.set(false);
        }

    }
}