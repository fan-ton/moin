package threads.moin.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.selection.Selection;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.window.layout.WindowMetrics;
import androidx.window.layout.WindowMetricsCalculator;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.navigationrail.NavigationRailView;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;

import java.util.Base64;
import java.util.Objects;

import threads.lite.cid.PeerId;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.boxes.BoxItemDetailsLookup;
import threads.moin.boxes.BoxItemKeyProvider;
import threads.moin.boxes.BoxViewAdapter;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.boxes.Box;
import threads.moin.core.events.EVENTS;
import threads.moin.model.MoinViewModel;


public class BoxesFragment extends Fragment implements BoxViewAdapter.ViewAdapterListener {

    private static final String TAG = BoxesFragment.class.getSimpleName();

    private static final int CLICK_OFFSET = 500;
    private final ActivityResultLauncher<ScanOptions>
            mScanRequestForResult = registerForActivityResult(new ScanContract(),
            result -> {
                if (result.getContents() != null) {
                    try {
                        String content = result.getContents();
                        Uri uri = Uri.parse(content);
                        Objects.requireNonNull(uri);
                        PeerId owner = PeerId.decode(uri.getAuthority());
                        String alias = uri.getQueryParameter(Content.ALIAS);
                        if (alias == null || alias.isEmpty()) {
                            alias = API.leftAlias(owner.toString());
                        } else {
                            alias = new String(Base64.getDecoder().decode(alias));
                        }
                        API.createBox(requireContext(), owner, alias);

                    } catch (Throwable ignore) {
                        EVENTS.getInstance(requireContext()).error(
                                getString(R.string.account_identification_not_valid));
                    }
                }
            });
    private final ActivityResultLauncher<String> requestPermissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                if (isGranted) {
                    invokeScan();
                } else {
                    EVENTS.getInstance(requireContext()).permission(
                            getString(R.string.permission_camera_denied));
                }
            });
    private long lastClickTime = 0;
    private MoinViewModel moinViewModel;
    private RecyclerView recyclerView;
    private BoxViewAdapter boxViewAdapter;
    private ActionMode actionMode;
    private SelectionTracker<Long> selectionTracker;
    private ExtendedFloatingActionButton extendedFloatingActionButton;
    private NavigationRailView navigationRailView;
    private boolean widthMode;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.boxes_view, container, false);
    }

    private void switchDisplayModes() {

        WindowMetrics metrics = WindowMetricsCalculator.getOrCreate()
                .computeCurrentWindowMetrics(requireActivity());

        float widthDp = metrics.getBounds().width() /
                getResources().getDisplayMetrics().density;
        widthMode = widthDp >= 600;


        if (!widthMode) {
            navigationRailView.setVisibility(View.GONE);
            extendedFloatingActionButton.show();
        } else {
            navigationRailView.setVisibility(View.VISIBLE);
            extendedFloatingActionButton.hide();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navigationRailView = view.findViewById(R.id.navigation_rail);

        navigationRailView.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.menu_scan_peer) {
                if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                    return true;
                }
                lastClickTime = SystemClock.elapsedRealtime();

                clickInvokeScan();
                return true;
            }
            return false;
        });


        recyclerView = view.findViewById(R.id.boxes);
        LinearLayoutManager linearLayout = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setItemAnimator(null);

        boxViewAdapter = new BoxViewAdapter(requireContext(), this);
        boxViewAdapter.setHasStableIds(true);
        recyclerView.setAdapter(boxViewAdapter);


        extendedFloatingActionButton = view.findViewById(R.id.fab_user);

        extendedFloatingActionButton.setOnClickListener((v) -> {

            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();

            clickInvokeScan();
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                boolean hasSelection = selectionTracker.hasSelection();
                if (dy > 0 && !hasSelection) {
                    showFab(false);
                } else if (dy < 0 && !hasSelection) {
                    showFab(true);
                }

            }
        });

        switchDisplayModes();

        moinViewModel = new ViewModelProvider(requireActivity())
                .get(MoinViewModel.class);
        moinViewModel.getShowFab().observe(getViewLifecycleOwner(), (showFab) -> {
            try {
                if (showFab != null) {
                    showFab(showFab);
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
        moinViewModel.getBoxes().observe(getViewLifecycleOwner(), (boxes) -> {

            try {
                if (boxes != null) {
                    int before = boxViewAdapter.getItemCount();
                    boxViewAdapter.updateData(boxes);

                    if (before != boxes.size()) {
                        recyclerView.scrollToPosition(0);
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });


        selectionTracker = new SelectionTracker.Builder<>(
                "boxes-selection",//unique id
                recyclerView,
                new BoxItemKeyProvider(boxViewAdapter),
                new BoxItemDetailsLookup(recyclerView),
                StorageStrategy.createLongStorage())
                .build();


        selectionTracker.addObserver(new SelectionTracker.SelectionObserver<>() {
            @Override
            public void onSelectionChanged() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionChanged();
            }

            @Override
            public void onSelectionRestored() {
                if (!selectionTracker.hasSelection()) {
                    if (actionMode != null) {
                        actionMode.finish();
                    }
                } else {
                    if (actionMode == null) {
                        actionMode = ((AppCompatActivity)
                                requireActivity()).startSupportActionMode(
                                createActionModeCallback());
                    }
                }
                if (actionMode != null) {
                    actionMode.setTitle("" + selectionTracker.getSelection().size());
                }
                super.onSelectionRestored();
            }

        });

        boxViewAdapter.setSelectionTracker(selectionTracker);


        if (savedInstanceState != null) {
            selectionTracker.onRestoreInstanceState(savedInstanceState);
        }
    }

    private long[] convert(Selection<Long> entries) {
        int i = 0;

        long[] basic = new long[entries.size()];
        for (Long entry : entries) {
            basic[i] = entry;
            i++;
        }

        return basic;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (selectionTracker != null) {
            selectionTracker.onSaveInstanceState(outState);
        }

    }

    public void showFab(boolean visible) {
        if (widthMode) {
            return;
        }
        if (visible) {
            extendedFloatingActionButton.show();
        } else {
            extendedFloatingActionButton.hide();
        }
    }

    private void clickInvokeScan() {

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
            return;
        }

        invokeScan();
    }

    private void invokeScan() {
        try {
            PackageManager pm = requireContext().getPackageManager();

            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                ScanOptions options = new ScanOptions();
                options.setDesiredBarcodeFormats(ScanOptions.ALL_CODE_TYPES);
                options.setPrompt(getString(R.string.scan_profile));
                options.setCameraId(0);  // Use a specific camera of the device
                options.setBeepEnabled(true);
                options.setOrientationLocked(false);
                mScanRequestForResult.launch(options);
            } else {
                EVENTS.getInstance(requireContext()).permission(
                        getString(R.string.feature_camera_required));
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    private ActionMode.Callback createActionModeCallback() {
        return new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_group_action_mode, menu);

                showFab(false);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.action_mode_delete) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                        return true;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();

                    deleteAction();

                    return true;
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

                selectionTracker.clearSelection();

                showFab(true);
                if (actionMode != null) {
                    actionMode = null;
                }

            }
        };

    }


    private void deleteAction() {

        try {
            long[] entries = convert(selectionTracker.getSelection());

            API.deleteBoxes(requireContext(), entries);

            selectionTracker.clearSelection();

            moinViewModel.setBox(0L);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onClick(@NonNull Box box) {

        try {
            if (SystemClock.elapsedRealtime() - lastClickTime < CLICK_OFFSET) {
                return;
            }
            lastClickTime = SystemClock.elapsedRealtime();
            moinViewModel.setBox(box.getIdx());
            moinViewModel.setAlias(box.getName());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }
}
