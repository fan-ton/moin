package threads.moin.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.util.Base64;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import threads.lite.IPFS;
import threads.lite.cid.IPV;
import threads.lite.cid.PeerId;
import threads.lite.core.Server;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.services.MimeTypeService;

public class ProfileDialogFragment extends BottomSheetDialogFragment {
    public static final String TAG = ProfileDialogFragment.class.getSimpleName();
    private static final int QR_CODE_SIZE = 250;
    private final AtomicBoolean notPrintErrorMessages = new AtomicBoolean(false);
    private TextInputLayout mEditNameLayout;
    private TextInputEditText mEditName;
    private ImageView mImageView;

    public static ProfileDialogFragment newInstance() {
        return new ProfileDialogFragment();
    }

    private static Bitmap getBitmap(@NonNull String content) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE, QR_CODE_SIZE);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            return barcodeEncoder.createBitmap(bitMatrix);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    private static Uri getProfileUri(@NonNull Context context, @NonNull String alias)
            throws Exception {

        IPFS ipfs = IPFS.getInstance(context);
        PeerId self = ipfs.self();
        Objects.requireNonNull(self);

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(Content.SCHEME)
                .authority(self.toString())
                .appendQueryParameter(Content.ALIAS,
                        Base64.getEncoder().encodeToString(alias.getBytes()));

        return builder.build();
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.getBehavior().setState(BottomSheetBehavior.STATE_EXPANDED);
        dialog.setContentView(R.layout.profile_view);

        mEditNameLayout = dialog.findViewById(R.id.text_name_layout);
        Objects.requireNonNull(mEditNameLayout);
        mEditNameLayout.setCounterEnabled(true);
        mEditNameLayout.setCounterMaxLength(30);

        mEditName = dialog.findViewById(R.id.text);
        Objects.requireNonNull(mEditName);
        mEditName.setText(API.getAlias(requireContext()));
        InputFilter[] filterTitle = new InputFilter[1];
        filterTitle[0] = new InputFilter.LengthFilter(30);
        mEditName.setFilters(filterTitle);

        mEditName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isValidName();
            }
        });

        mImageView = dialog.findViewById(R.id.image_pid);
        Objects.requireNonNull(mImageView);
        TextView important = dialog.findViewById(R.id.important);
        Objects.requireNonNull(important);
        TextView page = dialog.findViewById(R.id.page);
        Objects.requireNonNull(page);
        page.setMovementMethod(LinkMovementMethod.getInstance());
        page.setText(HtmlCompat.fromHtml("<a href='https://gitlab.com/remmer.wilts/moin'>"
                + getString(R.string.documentation)
                + "</a>", HtmlCompat.FROM_HTML_MODE_LEGACY));


        try {
            Server server = API.getInstance(requireContext()).getServer();
            Objects.requireNonNull(server);
            long port = server.getPort();
            Bitmap portBitmap = MimeTypeService.getPortBitmap(requireContext(), port);
            IPV ipv = IPFS.getInstance(requireContext()).ipv().get();
            Bitmap ipvBitmap = MimeTypeService.getIPvBitmap(requireContext(), ipv);

            Drawable ipvDrawable = new BitmapDrawable(getResources(), ipvBitmap);
            Drawable portDrawable = new BitmapDrawable(getResources(), portBitmap);
            important.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    ipvDrawable, null, portDrawable, null);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        loadImage(API.getAlias(requireContext()));

        dialog.setTitle(getString(R.string.profile));

        return dialog;
    }

    private void loadImage(@NonNull String name) {
        try {
            Uri uri = getProfileUri(requireContext(), name);
            Bitmap bitmap = getBitmap(uri.toString());
            mImageView.setImageBitmap(bitmap);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    private void isValidName() {

        Editable text = mEditName.getText();
        Objects.requireNonNull(text);
        String alias = text.toString();

        if (!notPrintErrorMessages.get()) {
            if (alias.isEmpty()) {
                mEditNameLayout.setError(getString(R.string.login_name_error));
            } else {
                mEditNameLayout.setError(null);
                updateName(alias);
            }

        } else {
            mEditNameLayout.setError(null);
            updateName(alias);
        }

    }

    private void updateName(@NonNull String alias) {
        API.setAlias(requireContext(), alias);
        loadImage(alias);
    }

    private void removeKeyboards() {
        try {
            InputMethodManager imm = (InputMethodManager)
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mEditName.getWindowToken(), 0);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        removeKeyboards();
    }

    @Override
    public void onResume() {
        super.onResume();
        notPrintErrorMessages.set(true);
        isValidName();
        notPrintErrorMessages.set(false);
    }
}
