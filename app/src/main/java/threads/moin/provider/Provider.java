package threads.moin.provider;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.OpenableColumns;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import threads.lite.cid.Cid;
import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.MimeType;

public class Provider {
    private static final String TAG = Provider.class.getSimpleName();

    private static final String DATA = "data";
    private static final String CACHE = "cache";
    private static volatile Provider INSTANCE = null;
    private final File dataDir;
    private final File cacheDir;

    private Provider(@NonNull Context context) {
        dataDir = new File(context.getFilesDir(), DATA);
        cacheDir = new File(context.getCacheDir(), CACHE);
    }

    public static Provider getInstance(@NonNull Context context) {

        if (INSTANCE == null) {
            synchronized (Provider.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Provider(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String getMimeType(@NonNull Context context, @NonNull Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        if (mimeType == null) {
            mimeType = MimeType.OCTET_MIME_TYPE;
        }
        return mimeType;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasReadPermission(@NonNull Context context, @NonNull Uri uri) {
        int perm = context.checkUriPermission(uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return perm != PackageManager.PERMISSION_DENIED;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean hasWritePermission(@NonNull Context context, @NonNull Uri uri) {
        int perm = context.checkUriPermission(uri, Binder.getCallingPid(), Binder.getCallingUid(),
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        return perm != PackageManager.PERMISSION_DENIED;
    }

    @NonNull
    public static String getFileName(@NonNull Context context, @NonNull Uri uri) {
        String filename = null;

        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
            filename = cursor.getString(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }

        if (filename == null) {
            filename = uri.getLastPathSegment();
        }

        if (filename == null) {
            filename = "file_name_not_detected";
        }

        return filename;
    }

    public static long getFileSize(@NonNull Context context, @NonNull Uri uri) {

        ContentResolver contentResolver = context.getContentResolver();

        try (Cursor cursor = contentResolver.query(uri,
                null, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();
            int nameIndex = cursor.getColumnIndex(OpenableColumns.SIZE);
            return cursor.getLong(nameIndex);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }


        try (ParcelFileDescriptor fd = contentResolver.openFileDescriptor(uri, "r")) {
            Objects.requireNonNull(fd);
            return fd.getStatSize();
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return -1;
    }

    public static boolean isPartial(@NonNull Context context, @NonNull Uri uri) {
        ContentResolver contentResolver = context.getContentResolver();
        try (Cursor cursor = contentResolver.query(uri, new String[]{
                DocumentsContract.Document.COLUMN_FLAGS}, null, null, null)) {

            Objects.requireNonNull(cursor);
            cursor.moveToFirst();

            int docFlags = cursor.getInt(0);
            if ((docFlags & DocumentsContract.Document.FLAG_PARTIAL) != 0) {
                return true;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return false;
    }

    @NonNull
    public static Uri getDataUri(@NonNull Context context, @NonNull File file) {
        return androidx.core.content.FileProvider.getUriForFile(
                context, API.AUTHORITY, file);
    }

    public File createTempDataFile() throws IOException {
        return File.createTempFile("tmp", ".data", getCacheDir());
    }

    public File getDataFile(@NonNull String filename) {
        return new File(getDataDir(), filename);
    }

    @NonNull
    public File createDataFile(@NonNull Cid cid) throws IOException {

        File file = getDataFile(cid.toString());
        if (file.exists()) {
            boolean result = file.delete();
            if (!result) {
                LogUtils.warning(TAG, "Deleting failed");
            }
        }
        boolean success = file.createNewFile();
        if (!success) {
            LogUtils.warning(TAG, "Failed create a new file");
        }
        return file;
    }


    @NonNull
    public File getCacheDir() {
        if (!cacheDir.isDirectory() && !cacheDir.exists()) {
            boolean result = cacheDir.mkdir();
            if (!result) {
                throw new RuntimeException("image directory does not exists");
            }
        }
        return cacheDir;
    }

    @NonNull
    public File getDataDir() {
        if (!dataDir.isDirectory() && !dataDir.exists()) {
            boolean result = dataDir.mkdir();
            if (!result) {
                throw new RuntimeException("image directory does not exists");
            }
        }
        return dataDir;
    }
}
