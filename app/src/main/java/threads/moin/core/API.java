package threads.moin.core;

import android.app.NotificationManager;
import android.content.ClipData;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cbor.CborObject;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Cancellable;
import threads.lite.core.Connection;
import threads.lite.core.IpnsRecord;
import threads.lite.core.Parameters;
import threads.lite.core.Server;
import threads.lite.core.Session;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.boxes.Box;
import threads.moin.core.events.EVENTS;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.provider.Provider;
import threads.moin.services.RequestService;
import threads.moin.work.UploadFileWorker;
import threads.moin.work.UploadFilesWorker;

public class API {
    public static final String AUTHORITY = "threads.moin.provider";
    public static final long PROGRESS_LIMIT_BYTES = 5000000; // 5 MB
    public static final int MAX_FILES = 50;
    private static final String TAG = API.class.getSimpleName();
    private static final String APP_KEY = "AppKey";
    private static final String ALIAS_KEY = "aliasKey";
    private static volatile API INSTANCE = null;
    @NonNull
    private final IPFS ipfs;
    @NonNull
    private final MAILS mails;
    @NonNull
    private final BOXES boxes;
    @NonNull
    private final Session session;

    private final Server server;

    private API(@NonNull Context context) throws Exception {
        ipfs = IPFS.getInstance(context);
        mails = MAILS.getInstance(context);
        boxes = BOXES.getInstance(context);
        session = ipfs.createSession(cid -> false); // default global session
        server = ipfs.startServer(7001, connection -> RequestService.incomingConnection(
                        context, connection),
                connection -> LogUtils.error(TAG, "Connect closed " +
                        connection.remoteMultiaddr()), peerId -> false);

    }

    public static void setAlias(@NonNull Context context, @NonNull String alias) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(ALIAS_KEY, alias);
        editor.apply();
    }

    @NonNull
    public static String getAlias(@NonNull Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(
                APP_KEY, Context.MODE_PRIVATE);
        return Objects.requireNonNull(sharedPref.getString(ALIAS_KEY, API.getDeviceName()));
    }

    public static API getInstance(@NonNull Context context) throws Exception {
        if (INSTANCE == null) {
            synchronized (API.class) {
                if (INSTANCE == null) {
                    INSTANCE = new API(context);
                }
            }
        }
        return INSTANCE;
    }

    @NonNull
    public static String leftAlias(final String name) {
        if (name == null) {
            return "";
        }
        if (name.length() <= 30) {
            return name;
        }
        return name.substring(0, 30);
    }

    @NonNull
    public static String getDeviceName() {
        try {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (model.startsWith(manufacturer)) {
                return API.leftAlias(capitalize(model));
            }
            return API.leftAlias(capitalize(manufacturer) + " " + model);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
        return "";
    }

    @NonNull
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase = phrase.concat("" + Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase = phrase.concat("" + c);
        }
        return phrase;
    }

    public static void files(@NonNull Context context, @NonNull ClipData data, long parent) {

        try {

            int items = data.getItemCount();

            if (items > 0) {
                Provider provider = Provider.getInstance(context);
                File file = provider.createTempDataFile();

                try (PrintStream out = new PrintStream(file)) {
                    for (int i = 0; i < items; i++) {
                        ClipData.Item item = data.getItemAt(i);
                        Uri uri = item.getUri();
                        out.println(uri.toString());

                        if (i == (API.MAX_FILES - 1)) {
                            EVENTS.getInstance(context).warning(
                                    context.getString(R.string.limitation_files, "" +
                                            API.MAX_FILES));
                            break;
                        }
                    }
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

                Uri uri = androidx.core.content.FileProvider.getUriForFile(
                        context, AUTHORITY, file);
                Objects.requireNonNull(uri);
                UploadFilesWorker.load(context, parent, uri);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public static void uploadFile(@NonNull Context context, @NonNull Uri uri, long box) {


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + uri);

        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                MAILS mails = MAILS.getInstance(context);

                String filename = Provider.getFileName(context, uri);
                long size = Provider.getFileSize(context, uri);
                String mimeType = Provider.getMimeType(context, uri);

                IPFS ipfs = IPFS.getInstance(context);
                PeerId host = ipfs.self();
                Objects.requireNonNull(host);

                long timestamp = System.currentTimeMillis();
                Mail mail = mails.createFileMail(box, host, timestamp);
                mail.setMimeType(mimeType);
                mail.setFilename(filename);
                mail.setSize(size);
                mail.setInit(true);
                mail.setText(filename);
                mail.setPublished(false);
                mail.setUri(uri.toString());
                long idx = mails.storeMail(mail);

                UploadFileWorker.upload(context, idx);

            } catch (Throwable e) {
                EVENTS.getInstance(context).error(context.getString(R.string.file_not_valid));

            } finally {
                LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
            }

        });
    }

    public static void createBox(@NonNull Context context, @NonNull PeerId owner,
                                 @NonNull String alias) {

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                BOXES boxes = BOXES.getInstance(context);
                MAILS mails = MAILS.getInstance(context);

                API api = API.getInstance(context);
                EVENTS events = EVENTS.getInstance(context);
                if (Objects.equals(api.self(), owner)) {
                    events.warning(context.getString(R.string.same_pid_like_host));
                    return;
                }
                Box box = boxes.getBoxByOwner(owner);
                if (box == null) {
                    box = boxes.createBox(owner);
                    box.setName(alias);

                    String content = context.getString(R.string.greetings, alias);
                    box.setMimeType(MimeType.PLAIN_MIME_TYPE);
                    box.setContent(content);
                    long idx = boxes.storeBox(box);

                    mails.createInfo(idx, owner, content, System.currentTimeMillis());

                } else {
                    String oldAlias = box.getName();
                    if (!Objects.equals(oldAlias, alias)) {
                        boxes.setBoxName(box.getIdx(), alias);

                        String content = context.getString(R.string.contact_renaming, oldAlias, alias);

                        boxes.setBoxContent(box.getIdx(), content, MimeType.PLAIN_MIME_TYPE);

                        mails.createInfo(box.getIdx(), owner, content, System.currentTimeMillis());
                    }
                }
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

    public static void deleteBoxes(@NonNull Context context, long... indices) {

        BOXES boxes = BOXES.getInstance(context);
        EVENTS events = EVENTS.getInstance(context);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                boxes.setBoxesDeleting(indices);

                events.delete(Arrays.toString(indices));

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

    }

    public static void markRead(@NonNull Context context, long box) {
        BOXES boxes = BOXES.getInstance(context);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {
            try {
                boxes.resetBoxesNumber(box);
                try {
                    NotificationManager notificationManager = (NotificationManager)
                            context.getSystemService(Context.NOTIFICATION_SERVICE);
                    notificationManager.cancel((int) box);
                } catch (Throwable throwable) {
                    LogUtils.error(TAG, throwable);
                }

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

    public static void storeText(@NonNull Context context, long box, @NonNull String text) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                API api = API.getInstance(context);
                MAILS mails = MAILS.getInstance(context);
                IPFS ipfs = IPFS.getInstance(context);
                BOXES boxes = BOXES.getInstance(context);


                PeerId host = ipfs.self();
                Objects.requireNonNull(host);

                long timestamp = System.currentTimeMillis();

                Mail mail = mails.createMessageMail(box, host, text, timestamp);
                mail.setSeeding(true);
                mail.setPublished(false);
                long idx = mails.storeMail(mail);

                boxes.setBoxContent(box, text, MimeType.PLAIN_MIME_TYPE);

                api.push(idx);
            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });

    }

    public static void storeAudio(@NonNull Context context, long box, @NonNull File file) {


        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.submit(() -> {

            try {
                API api = API.getInstance(context);
                IPFS ipfs = IPFS.getInstance(context);
                MAILS mails = MAILS.getInstance(context);
                BOXES boxes = BOXES.getInstance(context);

                PeerId host = ipfs.self();
                Objects.requireNonNull(host);

                String name = file.getName();
                String mimeType = "audio/m4a";
                long size = file.length();

                Cid cid = ipfs.storeFile(api.getSession(), file);
                Objects.requireNonNull(cid);
                long timestamp = System.currentTimeMillis();
                Mail mail = mails.createAudioMail(box, host,
                        mimeType, name, cid, size, timestamp);
                mail.setUri(Provider.getDataUri(context, file).toString());
                mail.setText("");
                mail.setSeeding(true);
                mail.setPublished(false);
                long idx = mails.storeMail(mail);

                boxes.setBoxContent(box, context.getString(R.string.audio), mimeType);


                api.push(idx);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        });
    }

    @NonNull
    public Server getServer() {
        return server;
    }

    @NonNull
    public Session getSession() {
        return session;
    }

    @NonNull
    public PeerId self() {
        return ipfs.self();
    }


    public void removeBox(@NonNull Box box) {
        removeBoxMails(box);
        boxes.removeBox(box);
    }

    public void removeBoxMails(@NonNull Box box) {
        for (Mail mail : mails.getMails(box)) {
            removeMail(mail);
        }
    }

    public void removeMail(@NonNull Mail mail) {
        mails.removeMail(mail.getIdx());
        unpin(mail.getCid());
    }

    public void unpin(@Nullable Cid cid) {
        try {
            if (cid != null) {
                if (!mails.isReferenced(cid)) {
                    ipfs.removeBlocks(session, cid);
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void notify(@NonNull PeerId peerId, long idx, byte[] data) {
        Connection connection = getSession().getConnection(peerId);
        if (connection != null && connection.isConnected()) {
            push(connection, idx, data);
        }
    }


    public void leave(@NonNull PeerId member) {
        try {
            SortedMap<CborObject, CborObject> map = new TreeMap<>(
                    Comparator.comparingInt(Object::hashCode));
            map.put(new CborObject.CborString(Content.EST),
                    new CborObject.CborString(FcmContent.LEAVE.name()));
            map.put(new CborObject.CborString(Content.TIME),
                    new CborObject.CborLong(System.currentTimeMillis()));
            CborObject.CborMap cborMap = new CborObject.CborMap(map);
            notify(member, -1, cborMap.toByteArray());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void push(@NonNull Mail mail) {
        byte[] push = Mail.pushFormat(mail);
        PeerId owner = boxes.getBoxOwner(mail.getBox());
        notify(owner, mail.getIdx(), push);
    }

    public void push(long idx) {
        try {
            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);
            push(mail);
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }


    public void received(@NonNull Connection connection, long ident) {
        try {
            SortedMap<CborObject, CborObject> map = new TreeMap<>(
                    Comparator.comparingInt(Object::hashCode));
            map.put(new CborObject.CborString(Content.EST),
                    new CborObject.CborString(FcmContent.RECEIVED.name()));
            map.put(new CborObject.CborString(Content.IDENT),
                    new CborObject.CborLong(ident));
            CborObject.CborMap cborMap = new CborObject.CborMap(map);
            push(connection, -1, cborMap.toByteArray());
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    public void push(@NonNull Connection connection, long idx, byte[] content) {
        try {
            IpnsRecord ipnsRecord = ipfs.createSelfSignedIpnsRecord(0, content, null);
            ipfs.push(connection, ipnsRecord).whenComplete((unused, throwable) -> {
                if (throwable == null) {
                    if (idx > 0) {
                        mails.setMailPublished(idx);
                    }
                } else {
                    LogUtils.error(TAG, throwable);
                }
            });
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        }
    }

    @Nullable
    public Connection connect(@NonNull PeerId peerId, @NonNull Cancellable cancellable) {


        // foreach peer, we allow it only to enter once
        synchronized (peerId.toString().intern()) {
            try {
                if (cancellable.isCancelled()) {
                    return null;
                }
                return ipfs.dial(session, peerId, Parameters.getDefault(
                                IPFS.GRACE_PERIOD_RESERVATION, true), cancellable)
                        .get(120, TimeUnit.SECONDS);

            } catch (Throwable throwable) {
                LogUtils.error(TAG, throwable);
            }
        }
        return null;
    }

}
