package threads.moin.core.mails;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import java.util.Comparator;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.UUID;

import threads.lite.cbor.CborObject;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.moin.core.Content;
import threads.moin.core.FcmContent;
import threads.moin.core.MimeType;


@androidx.room.Entity
public class Mail {


    @NonNull
    @ColumnInfo(name = "owner")
    @TypeConverters(PeerId.class)
    private final PeerId owner;
    @ColumnInfo(name = "box")
    private final long box;
    @NonNull
    @TypeConverters(Mail.class)
    @ColumnInfo(name = "type")
    private final Type type;
    @ColumnInfo(name = "date")
    private final long date;
    @PrimaryKey(autoGenerate = true)
    private long idx;
    @NonNull
    @ColumnInfo(name = "mimeType")
    private String mimeType;
    @NonNull
    @ColumnInfo(name = "text")
    private String text;
    @Nullable
    @TypeConverters(Cid.class)
    @ColumnInfo(name = "cid")
    private Cid cid;
    @ColumnInfo(name = "size")
    private long size;
    @NonNull
    @ColumnInfo(name = "filename")
    private String filename;
    @Nullable
    @ColumnInfo(name = "uri")
    private String uri;
    @ColumnInfo(name = "seeding")
    private boolean seeding;
    @ColumnInfo(name = "deleting")
    private boolean deleting;
    @ColumnInfo(name = "published")
    private boolean published;
    @ColumnInfo(name = "leaching")
    private boolean leaching;
    @ColumnInfo(name = "received")
    private boolean received;
    @Nullable
    @ColumnInfo(name = "work")
    private String work;
    @ColumnInfo(name = "init")
    private boolean init;
    @ColumnInfo(name = "ident")
    private long ident;


    Mail(long box, @NonNull PeerId owner, @NonNull Type type, long date) {
        this.box = box;
        this.owner = owner;
        this.mimeType = MimeType.OCTET_MIME_TYPE;
        this.type = type;
        this.published = false;
        this.leaching = false;
        this.filename = "";
        this.size = 0L;
        this.seeding = false;
        this.text = "";
        this.ident = 0;
        this.date = date;
        this.init = false;
        this.received = false;
    }

    public static Mail createMail(long box, @NonNull PeerId owner,
                                  @NonNull Type type, long date) {

        return new Mail(box, owner, type, date);
    }

    public static byte[] pushFormat(@NonNull Mail mail) {

        SortedMap<CborObject, CborObject> map = new TreeMap<>(
                Comparator.comparingInt(Object::hashCode));
        if (mail.getType() == Type.MESSAGE) {
            map.put(new CborObject.CborString(Content.EST),
                    new CborObject.CborString(FcmContent.MESSAGE.name()));
            map.put(new CborObject.CborString(Content.TXT),
                    new CborObject.CborString(mail.getText()));
        } else {
            map.put(new CborObject.CborString(Content.EST),
                    new CborObject.CborString(mail.getType().name()));
            Cid cid = mail.getCid();
            if (cid != null) {
                map.put(new CborObject.CborString(Content.CID),
                        new CborObject.CborString(cid.toString()));
            }

            map.put(new CborObject.CborString(Content.MIME_TYPE),
                    new CborObject.CborString(mail.getMimeType()));
            map.put(new CborObject.CborString(Content.FILENAME),
                    new CborObject.CborString(mail.getFilename()));

            if (!mail.getText().isEmpty()) {
                map.put(new CborObject.CborString(Content.TXT),
                        new CborObject.CborString(mail.getText()));
            }

            map.put(new CborObject.CborString(Content.FILESIZE),
                    new CborObject.CborLong(mail.getSize()));
        }
        map.put(new CborObject.CborString(Content.IDENT),
                new CborObject.CborLong(mail.getIdx()));
        map.put(new CborObject.CborString(Content.TIME),
                new CborObject.CborLong(mail.getDate()));
        CborObject.CborMap cborMap = new CborObject.CborMap(map);

        return cborMap.toByteArray();
    }

    @TypeConverter
    public static Type toNoteType(@NonNull Integer type) {

        if (type.equals(Type.MESSAGE.getCode())) {
            return Type.MESSAGE;
        } else if (type.equals(Type.INFO.getCode())) {
            return Type.INFO;
        } else if (type.equals(Type.AUDIO.getCode())) {
            return Type.AUDIO;
        } else if (type.equals(Type.FILE.getCode())) {
            return Type.FILE;
        } else {
            return Type.INFO;
        }
    }

    @TypeConverter
    public static Integer toInteger(@NonNull Type type) {

        return type.getCode();
    }

    @Nullable
    public Cid getCid() {
        return cid;
    }

    public void setCid(@Nullable Cid cid) {
        this.cid = cid;
    }

    public long getIdent() {
        return ident;
    }

    public void setIdent(long ident) {
        this.ident = ident;
    }

    public boolean isReceived() {
        return received;
    }

    public void setReceived(boolean received) {
        this.received = received;
    }

    public boolean isDeleting() {
        return deleting;
    }

    public void setDeleting(boolean deleting) {
        this.deleting = deleting;
    }

    @Nullable
    public String getWork() {
        return work;
    }

    public void setWork(@Nullable String work) {
        this.work = work;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    @Nullable
    public String getUri() {
        return uri;
    }

    public void setUri(@Nullable String uri) {
        this.uri = uri;
    }

    public long getDate() {
        return date;
    }

    public boolean isLeaching() {
        return leaching;
    }

    public void setLeaching(boolean leaching) {
        this.leaching = leaching;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    @NonNull
    public PeerId getOwner() {
        return owner;
    }

    @NonNull
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(@NonNull String mimeType) {
        this.mimeType = mimeType;
    }

    public long getIdx() {
        return idx;
    }

    void setIdx(long idx) {
        this.idx = idx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mail that = (Mail) o;
        return Objects.equals(idx, that.idx);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idx);
    }

    @NonNull
    public Type getType() {
        return type;
    }

    public boolean areItemsTheSame(@NonNull Mail mail) {
        return idx == mail.getIdx();
    }

    public long getBox() {
        return box;
    }

    public boolean sameContent(@NonNull Mail o) {

        if (this == o) return true;
        return type == o.getType() &&
                published == o.isPublished() &&
                received == o.isReceived() &&
                leaching == o.isLeaching() &&
                seeding == o.isSeeding() &&
                Objects.equals(getUri(), o.getUri()) &&
                Objects.equals(cid, o.getCid()) &&
                Objects.equals(work, o.getWork());
    }

    @Override
    @NonNull
    public String toString() {
        return "Mail{" +
                ", owner='" + owner + '\'' +
                ", box='" + box + '\'' +
                ", noteType=" + type +
                ", mimeType='" + mimeType + '\'' +
                ", idx=" + idx +
                ", cid='" + cid + '}';
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    @NonNull
    public String getFilename() {
        return filename;
    }

    public void setFilename(@NonNull String filename) {
        this.filename = filename;
    }

    public boolean isSeeding() {
        return seeding;
    }

    public void setSeeding(boolean seeding) {
        this.seeding = seeding;
    }

    @NonNull
    public String getText() {
        return text;
    }

    public void setText(@NonNull String text) {
        this.text = text;
    }

    public UUID getWorkUUID() {
        if (work != null) {
            return UUID.fromString(work);
        }
        return null;
    }

    public boolean isWorking() {
        return work != null;
    }

    public enum Type {
        MESSAGE(1), INFO(2), AUDIO(5), FILE(6);

        @NonNull
        private final Integer code;

        Type(@NonNull Integer code) {
            this.code = code;
        }


        @NonNull
        public Integer getCode() {
            return code;
        }

    }

}
