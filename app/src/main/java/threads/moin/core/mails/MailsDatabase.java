package threads.moin.core.mails;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;


@androidx.room.Database(entities = {Mail.class}, version = 1, exportSchema = false)
@TypeConverters({PeerId.class, Cid.class, Mail.class})
public abstract class MailsDatabase extends RoomDatabase {

    public abstract MailDao mailDao();


}
