package threads.moin.core.mails;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;

@Dao
public interface MailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertMail(Mail mail);


    @Query("UPDATE Mail SET cid = null WHERE idx = :idx")
    void unsetContent(long idx);

    @Query("Delete FROM Mail WHERE idx =:idx")
    void removeMailByIdx(long idx);

    @Query("SELECT * FROM Mail WHERE idx =:idx")
    Mail getMailByIdx(long idx);

    @Query("SELECT * FROM Mail WHERE box =:box")
    List<Mail> getMailsByBox(long box);

    @Query("SELECT * FROM Mail WHERE box =:box AND deleting = 0 AND (text LIKE :query) ORDER BY date DESC")
    LiveData<List<Mail>> getLiveDataMails(long box, String query);

    @Query("SELECT COUNT(idx) FROM Mail WHERE cid =:cid")
    int references(Cid cid);

    @Query("UPDATE Mail SET seeding = 1, leaching = 0, init = 0, work = null  WHERE idx = :idx")
    void setSeeding(long idx);

    @Query("UPDATE Mail SET cid = :cid, seeding = 1, leaching = 0, init = 0, work = null WHERE idx = :idx")
    void setSeeding(long idx, Cid cid);

    @Query("UPDATE Mail SET work = :work, leaching = 1 , init = 0 WHERE idx = :idx")
    void setLeaching(long idx, String work);

    @Query("UPDATE Mail SET work = null, leaching = 0, init = 1  WHERE idx = :idx")
    void resetLeaching(long idx);

    @Query("UPDATE Mail SET published = 1 WHERE idx = :idx")
    void setPublished(long idx);

    @Query("UPDATE Mail SET received = 1, published = 1, ident = 0  WHERE idx = :idx")
    void setReceived(long idx);

    @Query("UPDATE Mail SET size = :size WHERE idx = :idx")
    void setSize(long idx, long size);

    @Query("SELECT * FROM Mail WHERE owner =:owner AND published = 0 AND box =:box")
    List<Mail> getBoxNotifications(PeerId owner, long box);

    @Query("SELECT * FROM Mail WHERE deleting = 1")
    List<Mail> getDeletedMails();

    @Query("UPDATE Mail SET uri = :uri, cid = null WHERE idx = :idx")
    void setLocal(long idx, String uri);

}
