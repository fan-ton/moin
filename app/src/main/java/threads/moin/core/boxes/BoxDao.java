package threads.moin.core.boxes;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import threads.lite.cid.PeerId;

@Dao
public interface BoxDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertBox(Box box);

    @Query("SELECT * FROM Box WHERE deleting = 0 AND name LIKE :query ORDER BY date DESC")
    LiveData<List<Box>> getLiveDataBoxesByQuery(String query);

    @Query("SELECT * FROM Box WHERE owner = :owner")
    Box getBoxByOwner(PeerId owner);

    @Delete
    void removeBox(Box box);

    @Query("UPDATE Box SET number = 0 WHERE idx = :idx")
    void resetNumber(long idx);

    @Query("UPDATE Box SET number = number + 1 WHERE idx = :idx")
    void incrementNumber(long idx);

    @Query("SELECT * FROM Box WHERE idx =:idx")
    Box getBoxByIdx(long idx);

    @Query("UPDATE Box SET content =:content, mimeType =:mimeType, date = :date  WHERE idx = :idx")
    void setContent(long idx, String content, String mimeType, long date);

    @Query("UPDATE Box SET name =:name, date = :date  WHERE idx = :idx")
    void setName(long idx, String name, long date);

    @Query("SELECT * FROM Box WHERE deleting = 1")
    List<Box> getDeletedBoxes();

    @Query("UPDATE Box SET deleting = 0 WHERE idx IN (:idxs)")
    void resetBoxesDeleting(long[] idxs);

    @Query("UPDATE Box SET deleting = 1 WHERE idx IN (:idxs)")
    void setBoxesDeleting(long[] idxs);

    @Query("SELECT owner FROM Box  WHERE idx = :idx")
    PeerId getOwner(long idx);

    @Query("SELECT (owner) FROM Box WHERE deleting = 0")
    List<PeerId> getSwarm();
}
