package threads.moin.core.boxes;

import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import threads.lite.cid.PeerId;


@androidx.room.Database(entities = {Box.class}, version = 1, exportSchema = false)
@TypeConverters({PeerId.class})
public abstract class BoxesDatabase extends RoomDatabase {

    public abstract BoxDao boxDao();
}
