package threads.moin.core;


public class Content {
    public static final String SCHEME = "moin";
    public static final String EST = "est";
    public static final String ALIAS = "alias";
    public static final String CID = "cid";
    public static final String MIME_TYPE = "type";
    public static final String FILENAME = "fn";
    public static final String FILESIZE = "fs";
    public static final String IDX = "idx";
    public static final String BOX = "box";
    public static final String URI = "uri";
    public static final String IDXS = "idxs";
    public static final String TXT = "txt";
    public static final String TIME = "time";
    public static final String IDENT = "ident";
}
