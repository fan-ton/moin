package threads.moin.core;

import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.view.View;

public class PermissionAction implements View.OnClickListener {


    @Override
    public void onClick(View v) {

        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse("package:" + v.getContext().getPackageName()));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        v.getContext().startActivity(intent);
    }
}
