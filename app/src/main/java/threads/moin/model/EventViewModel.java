package threads.moin.model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import threads.moin.core.events.EVENTS;
import threads.moin.core.events.Event;
import threads.moin.core.events.EventsDatabase;

public class EventViewModel extends AndroidViewModel {

    private final EventsDatabase eventsDatabase;

    public EventViewModel(@NonNull Application application) {
        super(application);
        eventsDatabase = EVENTS.getInstance(
                application.getApplicationContext()).getEventsDatabase();
    }

    public LiveData<Event> permission() {
        return eventsDatabase.eventDao().getEvent(EVENTS.PERMISSION);
    }

    public LiveData<Event> delete() {
        return eventsDatabase.eventDao().getEvent(EVENTS.DELETE);
    }

    public LiveData<Event> error() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ERROR);
    }

    public LiveData<Event> fatal() {
        return eventsDatabase.eventDao().getEvent(EVENTS.FATAL);
    }

    public LiveData<Event> offline() {
        return eventsDatabase.eventDao().getEvent(EVENTS.OFFLINE);
    }

    public LiveData<Event> online() {
        return eventsDatabase.eventDao().getEvent(EVENTS.ONLINE);
    }

    public LiveData<Event> warning() {
        return eventsDatabase.eventDao().getEvent(EVENTS.WARNING);
    }

    public void removeEvent(@NonNull final Event event) {
        eventsDatabase.eventDao().deleteEvent(event);
    }

}