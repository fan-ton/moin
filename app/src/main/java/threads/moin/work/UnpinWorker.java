package threads.moin.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.Objects;

import threads.lite.cid.Cid;
import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;

public class UnpinWorker extends Worker {

    private static final String TAG = UnpinWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public UnpinWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UnpinWorker.class)
                .addTag(UnpinWorker.TAG)
                .setInputData(data.build())
                .build();
    }

    public static void unpin(@NonNull Context context, long idx) {
        WorkManager.getInstance(context).enqueue(getWork(idx));
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);


        try {
            API api = API.getInstance(getApplicationContext());
            MAILS mails = MAILS.getInstance(getApplicationContext());
            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);

            Cid cid = mail.getCid();
            if (cid != null) {
                mails.unsetMailContent(idx); // unset not content (cid == null)
                api.unpin(cid);
            }

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }

}
