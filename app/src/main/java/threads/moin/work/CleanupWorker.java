package threads.moin.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.TimeUnit;

import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.mails.MAILS;


public class CleanupWorker extends Worker {
    private static final String TAG = CleanupWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public CleanupWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork() {
        return new OneTimeWorkRequest.Builder(CleanupWorker.class)
                .addTag(TAG)
                .setInitialDelay(5, TimeUnit.MINUTES)
                .build();

    }

    public static void cleanup(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                TAG, ExistingWorkPolicy.KEEP, getWork());

    }


    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {

            API api = API.getInstance(getApplicationContext());


            BOXES boxes = BOXES.getInstance(getApplicationContext());
            MAILS mails = MAILS.getInstance(getApplicationContext());

            boxes.getDeletedBoxes().forEach(api::removeBox);
            mails.getDeletedMails().forEach(api::removeMail);

        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }
}

