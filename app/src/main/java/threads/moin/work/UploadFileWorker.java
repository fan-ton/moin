package threads.moin.work;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.InputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;

public class UploadFileWorker extends Worker {
    private static final String TAG = UploadFileWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public UploadFileWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);

        return new OneTimeWorkRequest.Builder(UploadFileWorker.class)
                .addTag(UploadFileWorker.TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void upload(@NonNull Context context, long idx) {
        OneTimeWorkRequest request = getWork(idx);
        WorkManager.getInstance(context).enqueue(request);
    }

    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);


        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        MAILS mails = MAILS.getInstance(getApplicationContext());
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());
            BOXES boxes = BOXES.getInstance(getApplicationContext());

            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);

            mails.setLeaching(idx, getId());

            String uri = mail.getUri();
            Objects.requireNonNull(uri);

            try (InputStream inputStream = getApplicationContext().getContentResolver().
                    openInputStream(Uri.parse(uri))) {

                Objects.requireNonNull(inputStream);

                Cid cid = ipfs.storeInputStream(api.getSession(), inputStream);

                Objects.requireNonNull(cid);
                mails.setSeeding(idx, cid);

                boxes.setBoxContent(mail.getBox(), mail.getText(), mail.getMimeType());

                api.push(idx);

            } catch (Throwable throwable) {
                mails.removeMail(idx);
                throw throwable;
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();
    }


}
