package threads.moin.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.ForegroundInfo;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.cid.PeerId;
import threads.lite.core.Progress;
import threads.moin.InitApplication;
import threads.moin.LogUtils;
import threads.moin.MainActivity;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.boxes.BOXES;
import threads.moin.core.events.EVENTS;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;
import threads.moin.provider.Provider;

public class UploadFilesWorker extends Worker {

    private static final String TAG = UploadFilesWorker.class.getSimpleName();


    @SuppressWarnings("WeakerAccess")
    public UploadFilesWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(long box, @NonNull Uri uri) {

        Data.Builder data = new Data.Builder();
        data.putString(Content.URI, uri.toString());
        data.putLong(Content.BOX, box);

        return new OneTimeWorkRequest.Builder(UploadFilesWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .build();
    }

    public static void load(@NonNull Context context, long box, @NonNull Uri uri) {
        WorkManager.getInstance(context).enqueue(getWork(box, uri));
    }

    @NonNull
    @Override
    public Result doWork() {


        String uriFile = getInputData().getString(Content.URI);
        long box = getInputData().getLong(Content.BOX, 0L);
        long start = System.currentTimeMillis();

        LogUtils.info(TAG, " start ...");

        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());
            MAILS mails = MAILS.getInstance(getApplicationContext());

            PeerId host = ipfs.self();
            Objects.requireNonNull(host);
            BOXES boxes = BOXES.getInstance(getApplicationContext());
            Objects.requireNonNull(uriFile);

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);


            Notification.Builder builder = new Notification.Builder(
                    getApplicationContext(), InitApplication.MESSAGE_CHANNEL_ID);

            PendingIntent intent = WorkManager.getInstance(getApplicationContext())
                    .createCancelPendingIntent(getId());
            String cancel = getApplicationContext().getString(android.R.string.cancel);

            Intent main = new Intent(getApplicationContext(), MainActivity.class);

            int requestID = (int) System.currentTimeMillis();
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), requestID,
                    main, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);

            Notification.Action action = new Notification.Action.Builder(
                    Icon.createWithResource(getApplicationContext(), R.drawable.pause), cancel,
                    intent).build();

            builder.setContentTitle(getApplicationContext().getString(R.string.uploading))
                    .setContentIntent(pendingIntent)
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .addAction(action)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true)
                    .setOngoing(true);

            int notificationId = getId().hashCode();

            Notification notification = builder.build();
            notificationManager.notify(notificationId, notification);
            setForegroundAsync(new ForegroundInfo(notificationId, notification));


            List<String> uris = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    getApplicationContext().getContentResolver()
                            .openInputStream(Uri.parse(uriFile))))) {
                Objects.requireNonNull(reader);
                while (reader.ready()) {
                    uris.add(reader.readLine());
                }
            }

            int maxIndex = uris.size();
            AtomicInteger index = new AtomicInteger(0);
            for (String uriStr : uris) {
                Uri uri = Uri.parse(uriStr);
                if (!isStopped()) {
                    if (!Provider.hasReadPermission(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getApplicationContext().getString(
                                        R.string.file_has_no_read_permission));
                        continue;
                    }

                    if (Provider.isPartial(getApplicationContext(), uri)) {
                        EVENTS.getInstance(getApplicationContext()).error(
                                getApplicationContext().getString(R.string.file_not_valid));
                        continue;
                    }

                    int indexValue = index.incrementAndGet();

                    String name = Provider.getFileName(
                            getApplicationContext(), uri);
                    String mimeType = Provider.getMimeType(
                            getApplicationContext(), uri);


                    final long size = Provider.getFileSize(
                            getApplicationContext(), uri);

                    long timestamp = System.currentTimeMillis();
                    long idx = createFile(mails, host, box, name, mimeType, size, uri, timestamp);


                    try (InputStream inputStream = getApplicationContext().getContentResolver().
                            openInputStream(uri)) {

                        Objects.requireNonNull(inputStream);
                        Cid cid = ipfs.storeInputStream(api.getSession(), inputStream, new Progress() {
                            @Override
                            public boolean isCancelled() {
                                return isStopped();
                            }

                            @Override
                            public void setProgress(int progress) {
                                builder.setSubText("" + indexValue + "/" + maxIndex)
                                        .setContentTitle(name)
                                        .setProgress(100, progress, false);
                                notificationManager.notify(notificationId, builder.build());
                            }
                        }, size);

                        Objects.requireNonNull(cid);
                        mails.setSeeding(idx, cid);

                        boxes.setBoxContent(box, name, mimeType);

                        api.push(idx);


                    } catch (Throwable throwable) {
                        mails.removeMail(idx);
                        throw throwable;
                    }
                }
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }


    private long createFile(MAILS mails, PeerId host, long box, @NonNull String filename,
                            @NonNull String mimeType, long size, @NonNull Uri uri, long timestamp) {

        Mail mail = mails.createFileMail(box, host, timestamp);
        mail.setMimeType(mimeType);
        mail.setFilename(filename);
        mail.setLeaching(true);
        mail.setSize(size);
        mail.setInit(true);
        mail.setText(filename);
        mail.setPublished(false);
        mail.setUri(uri.toString());
        return mails.storeMail(mail);
    }
}
