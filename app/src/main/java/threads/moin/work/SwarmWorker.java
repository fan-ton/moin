package threads.moin.work;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.PeerId;
import threads.lite.core.Server;
import threads.lite.utils.TimeoutCancellable;
import threads.moin.LogUtils;
import threads.moin.core.API;
import threads.moin.core.boxes.BOXES;


public class SwarmWorker extends Worker {
    private static final String TAG = SwarmWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public SwarmWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

    }

    private static OneTimeWorkRequest getWork() {
        return new OneTimeWorkRequest.Builder(SwarmWorker.class)
                .addTag(SwarmWorker.class.getSimpleName()).build();
    }


    public static void connect(@NonNull Context context) {
        WorkManager.getInstance(context).enqueueUniqueWork(
                SwarmWorker.class.getSimpleName(), ExistingWorkPolicy.KEEP, getWork());
    }

    @NonNull
    @Override
    public Result doWork() {

        long start = System.currentTimeMillis();
        try {
            IPFS ipfs = IPFS.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());
            Server server = api.getServer();

            ExecutorService executor = Executors.newFixedThreadPool(
                    Runtime.getRuntime().availableProcessors());
            BOXES boxes = BOXES.getInstance(getApplicationContext());

            for (PeerId peerId : boxes.getSwarm()) {
                executor.execute(() -> api.connect(peerId, this::isStopped));
            }
            executor.shutdown();


            ipfs.reservations(server, new TimeoutCancellable(this::isStopped, 180));


            boolean termination = executor.awaitTermination(
                    10, TimeUnit.MINUTES);
            if (!termination) {
                executor.shutdownNow();
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.error(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }

        return Result.success();
    }


}

