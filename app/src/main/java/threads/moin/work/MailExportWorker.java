package threads.moin.work;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.OutputStream;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import threads.lite.IPFS;
import threads.lite.cid.Cid;
import threads.lite.core.Progress;
import threads.moin.InitApplication;
import threads.moin.LogUtils;
import threads.moin.R;
import threads.moin.core.API;
import threads.moin.core.Content;
import threads.moin.core.mails.MAILS;
import threads.moin.core.mails.Mail;

public class MailExportWorker extends Worker {
    private static final String TAG = MailExportWorker.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public MailExportWorker(@NonNull Context context,
                            @NonNull WorkerParameters params) {
        super(context, params);
    }


    private static OneTimeWorkRequest getWork(@NonNull Uri uri, long idx) {

        Data.Builder data = new Data.Builder();
        data.putLong(Content.IDX, idx);
        data.putString(Content.URI, uri.toString());

        return new OneTimeWorkRequest.Builder(MailExportWorker.class)
                .addTag(TAG)
                .setInputData(data.build())
                .setInitialDelay(1, TimeUnit.MILLISECONDS)
                .build();
    }

    public static void export(@NonNull Context context, @NonNull Uri uri, long idx) {
        WorkManager.getInstance(context).enqueue(getWork(uri, idx));
    }


    @NonNull
    @Override
    public Result doWork() {

        long idx = getInputData().getLong(Content.IDX, -1);
        String uri = getInputData().getString(Content.URI);
        Objects.requireNonNull(uri);

        long start = System.currentTimeMillis();
        LogUtils.info(TAG, " start ... " + idx);

        try {
            MAILS mails = MAILS.getInstance(getApplicationContext());
            API api = API.getInstance(getApplicationContext());
            IPFS ipfs = IPFS.getInstance(getApplicationContext());

            Mail mail = mails.getMailByIdx(idx);
            Objects.requireNonNull(mail);

            Cid cid = mail.getCid();
            Objects.requireNonNull(cid);

            NotificationManager notificationManager = (NotificationManager)
                    getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

            Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                    InitApplication.MESSAGE_CHANNEL_ID);


            builder.setContentText(mail.getFilename())
                    .setProgress(100, 0, false)
                    .setOnlyAlertOnce(true)
                    .setSmallIcon(R.drawable.download)
                    .setCategory(Notification.CATEGORY_PROGRESS)
                    .setUsesChronometer(true)
                    .setOngoing(true);

            int notificationId = getId().hashCode();
            notificationManager.notify(notificationId, builder.build());


            try (OutputStream os = getApplicationContext().getContentResolver().
                    openOutputStream(Uri.parse(uri))) {

                Objects.requireNonNull(os);
                ipfs.fetchToOutputStream(api.getSession(), os, cid, new Progress() {

                    @Override
                    public void setProgress(int progress) {
                        builder.setSubText("" + progress + "%")
                                .setProgress(100, progress, false);
                        notificationManager.notify(notificationId, builder.build());
                    }

                    @Override
                    public boolean isCancelled() {
                        return isStopped();
                    }
                });


            } finally {
                notificationManager.cancel(notificationId);
            }
        } catch (Throwable throwable) {
            LogUtils.error(TAG, throwable);
        } finally {
            LogUtils.info(TAG, " finish onStart [" + (System.currentTimeMillis() - start) + "]...");
        }
        return Result.success();

    }
}
