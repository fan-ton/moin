# Privacy Policy

## Data Protection

<p>As an application provider, we take the protection of all personal data very seriously.
All personal information is treated confidentially and in accordance with the legal requirements,
regulations, as explained in this privacy policy.</p>
<p>This app is designed so that the user do not have to enter any personal data. Never will data
collected by us, and especially not passed to third parties. The users behaviour is also not
analyzed by this application.</p>
<p>The user is responsible what kind of data is added or retrieved from the IPFS network.
This kind of information is also not tracked by this application.</p>

### Android Permissions

This section describes briefly why specific Android permissions are required.

- __Camera__
    - The camera permission is required to read QR codes from others users.
    - The camera permission is required so that the user can share image and video content.
- __Record Audio__
    - The audio record permission is required so that the user can share audio content.
- __Foreground Service__
    - The foreground service permission is required to run your service over a longer
      period of time. This is useful for providing your content to others and to receive
      notifications.
- __Internet__
    - Precondition for running the your decentralized service.

### Contact Information

<p>Remmer Wilts, Dr. Munderloh Str.10, 27798 Wüsting, Germany</p>